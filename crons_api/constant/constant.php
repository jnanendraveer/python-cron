<?php

//--------------About Products Start-------------------------//
define('GYM_PLAN_PREMIUM_TEXT', 'Premium Plan');
define('GYM_PLAN_BASIC_TEXT', 'Basic Plan');
define('MEMBERSHIP_PLAN_BASIC_TEXT', "FP Basic");
define('MEMBERSHIP_PLAN_LAITE_TEXT', "FP Lite");
define('MEMBERSHIP_PLAN_PRO_TEXT', "FP Pro");

define('MEMBERSHIP_PLAN_BASIC_DETAIL', "Any one of FITPASS, FITFEAST and FITCOACH");
define('MEMBERSHIP_PLAN_LAITE_DETAIL', "Any two of FITPASS, FITFEAST and FITCOACH");
define('MEMBERSHIP_PLAN_PRO_DETAIL', "Power pack of FITPASS, FITFEAST and FITCOACH");
define('REGISTRATION_REFERRAL_PRICE', 100); //Registration Referral price and If user will prime then user will get double credit
define('MEMBERSHIP_PLAN_BASIC_INT', "1");
define('MEMBERSHIP_PLAN_LAITE_INT', "2");
define('MEMBERSHIP_PLAN_PRO_INT', "3");
define('QUERY_TYPE_FOLLOW', "User Follow");
define('QUERY_TYPE_ADD_UPDATE_BOUGHT_HISTORY', "Add/Update custommer bought history");
define('QUERY_TYPE_CHANGES', "Updates");
define('STUDIO_ADMIN_ROLE', "1");
define('STUDIO_MANAGER_ROLE', "2");
define('USER_RESERVE_SCHEDULE', "Reserve schedule update");
define('USER_RESERVE_REMARKS', "Backend team add remarks");
define('USER_SUBSCRIPTION_CYCLE', "Backend team changed subscription");
define('REMARKS_TEXT', "Something update with remarks");
define('QUERY_TYPE_UPDATE_MEMEBRSHIP', "Change in memebrship");
define('QUERY_TYPE_UPDATE_SUBCRIPTIONS', "Change in user subcription");
define('QUERY_TYPE_STUDIO_PROBLEM', "Problem query by studio");
define('FITPASS_BACKEND', "FITPASS Backend");
define('MEMBERSHIP_ACTIVE', 1);
define('MEMBERSHIP_EXPIRED', 2);
define('MEMBERSHIP_DONT_BUY', 3);
define("DIET_TEAM_MOBILE_NUMBER", "9999374792,9818520883");

/* blog type start */
define('BLOG_TYPE_FITNESS', 1);
define('BLOG_TYPE_NUTRITION', 2);
define('BLOG_TYPE_SUCCESS_BLOG', 3);
/* blog type end */

/* For In Users Table Membership Plan Master Id */
define('USER_MEMBERSHIP_GENERAL', 1);
define('USER_MEMBERSHIP_MAGNUM', 2);
define('USER_MEMBERSHIP_CORPORATE', 3);
define('USER_MEMBERSHIP_MAGNUM_PRIMARY', 5);
define('USER_MEMBERSHIP_MAGNUM_SUPPLEMENT', 4);
/* For In Users Table Membership Plan Master Id end */

/* CMS page type start */
define('CMS_PAGE_ABOUT', 1);
define('CMS_PAGE_PRIVACY_POLICY', 2);
define('CMS_PAGE_TERM_AND_CONDITIONS', 3);
/* CMS page type end */

/* FAQ type start */
define('FAQ_TYPE_FITPASS_INT', 1);
define('FAQ_TYPE_NUTRITION_INT', 2);
define('FAQ_TYPE_FITCOACH_INT', 3);
define('FAQ_TYPE_PRIME_INT', 4);
define('FAQ_TYPE_FITSHOP_INT', 5);
/* FAQ type end */

define('FITPRIME_PLAN_NAME', "FITPRIME");
define('FITPRIME_CONTENT_DETAIL', "Get freebies, enhanced savings and more on all FITPASS offerings from Day 1.");
define('FITPRIME_CONTENT_PRICE', 499);
define('ZERO_VALUE', 0);

define('FITPASS', 'fitpass-');
define('FITPASS_MEMBERSHIP_ID', '1');
define('FITPASS_CUSTOMER_CARE', '011-46061468');
define('MAX_REDEEM_LIMIT_FITSHOP', 25);
define('MAX_REDEEM_LIMIT_OVER_ALL', 25);
define('MAX_REDEEM_LIMIT_FITCARD', 25);
define('GST_FITSHOP', 28);
define('GST_FITSHOP_OVER_ALL', 28);
define('GST_ON_MEMBERSHIP', 28);
define('GST_ON_MAGNUM_CARD', 18);
define('MAGNUM_PRODUCT_PRICE', '25000.00');
define('MAGNUM_MEMBERSHIP_ID', '57');
define('GOOGLE_SHORTNER_URL_API_KEY', 'AIzaSyB8a-d9GnjpQ_0BGGNHUO9lYUdD7wtQaEw');
//------------END------------------------------------------//
//---------------Emailer Subject & SMS Start---------------------------------//
define('STUDIO_EMAIL_REGISTRATION', 'Welcome to fitpass');
define('PRODUCT_EMAIL_ORDER_CONFIRMATION', "Order confirmation");
define('PRODUCT_EMAIL_ORDER_CANCELATION', "Order cancellation");
define('PRODUCT_EMAIL_ORDER_DELIVERED', 'Product deliver');
define('PRODUCT_EMAIL_ORDER_SHIPPED', 'Product shipped');
define('CHNAGE_PASSWORD', "Password Reset Request");
define('USER_REGISTRATION', "Welcome to FITPASS");
define('USER_BOUGHT_MEMBERSHIP', "Welcome to the FITPASS community");
define('LIMITED_WORKOUTS', 1);
define('UNLIMITED_WORKOUTS', 2);
define('USER_BOUGHT_FITCOACH_MEMBERSHIP', "Welcome to the FITPASS community");
define('USER_BOUGHT_FITPRIME', "Welcome to FITPASS");
define('USER_BOUGHT_DIET_MEMBERSHIP', "Welcome to healthy eating");
define('DIET_ASIGN_SUCCESS', "Your diet is ready! ");
define('WORKOUT_RESERVATION', "Workout Reservation Successful | ");
define('WORKOUT_CANCELATION', "Workout Cancelled Successful | ");
define("WORKOUT_RESERVE_SMS", 'Dear Member, Your booking details:WORKOUT_DETAILS, MEM ID: MEMBERSHIP_ID, URC: URC_CODE');
define('WORKOUT_CANCEL_NOTICE', 'Please note: You will not be able to cancel this reservation. Reservation can only be canceled 1.5 hours before workout time.');
define('CANCEL_SCHEDULE_MESSAGE', "Schedule cancel by studio");

define('MEMBERSHIP_RENEW_REMINDER', "Monthly Renewal Reminder");
define('FIT_DIRECT_DETAILS_TEXT', "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.");
define('FIT_CARD_DETAILS_TEXT', "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.");
define('STUDIO_WORKOUT_RESERVATION_CONFIRMATION', "Attendee List | ");
define('STUDIO_PAYMENT_CONFIRMATION', 'Payment from FITPASS - ASR Market Ventures Private Limited.');
define('STUDIO_PAYMNET_CONFIRMATION_SMS_CONTENT', 'Dear Partner,
Payment for your studio has been processed. Kindly check if you have received payment from ASR Market Ventures Pvt. Ltd.');

//---------------Emailer Subject Start---------------------------------//
//---------------Product URL Start---------------------------------//
define('SCHEDULE_SMS_API_URL', "https://alerts.kaleyra.com/api/v4/?api_key=A1eb55f4c0984231765145b441ba8f7a7&method=sms&message=message_text&to=mobile_numer&sender=FITPAS&time=TIME");
define('SMS_API_URL', 'https://alerts.kaleyra.com/api/v4/?api_key=A1eb55f4c0984231765145b441ba8f7a7&method=sms&message=message_text&to=mobile_numer&sender=FITPAS');
define('SMS_API_URL_OTP', 'https://otp2.maccesssmspush.com/OTP_ACL_Web/OtpRequestListener?enterpriseid=asrotp&subEnterpriseid=asrotp&pusheid=asrotp&pushepwd=asrotp20&msisdn=mobile_numer&sender=FITPAS&msgtext=message_text');
define('ASR_USER_SMS_API_URL', "https://alerts.kaleyra.com/api/v4/?api_key=Af086bd5f1c5eda10e775ef919c800dab&method=sms&message=message_text&to=mobile_numer&sender=FITPAS");
define('SITE_URL', 'https://fitpass.co.in');
define('STUDIOS_SITE_URL', 'http://studios.fitpass.co.in');
define('FITFEAST_MONTH_DESCRIPTION', "Get guaranteed results with real time assistance from expert nutritionist");
define('FITCOACH_PLAN_DESCRIPTION', "Revolutionary, A.I. enabled training plans that evolve and adapt to your needs!");
//.................Product End---------------------------------//
//--------------AWS placeho'product_type' => PRODUCT_TYPE_GYMMEMBERSHIP,lder for image upload start-----------------------------//
define('CUSTOMER_IMAGE', 'customer_');
define('VENDOR_IMAGE', 'vendor_');
define('FITDIRECT_TRAINER_IMAGE', 'fitdirect_trainer_');
define('TEAM_IMAGE', 'team_');
define('FRANCHISE_T_N_C', 'studio_termsagreed_doc_');
define('FRANCHISE_LOGO', 'studio_termsagreed_doc_');
define('STUDIO_LOGO', 'studio_logo_');
define('STUDIO_PROFILE', 'studio_profile_');
define('WORKOUT_IMAGE', 'workout_');
define('PRODUCT_IMAGE', 'product_');
define('OFFER_BANNER_IMAGE', 'offer_banner_');
define('DEALS_IMAGE', 'deal_image_');
define('NOTIFICATION_IMAGE', 'notificaition_');
define('FITCOACH_IMAGE', 'fitcoach_');
define('WORKOUTVIDEOTHUMBNAIL', 'workout_video_thumbnail_');
define('FITCARDPHOTO', 'fitcard_photo_');
define('BLOG_BANNER_IMAGE', 'blog_banner_');
define('CMS_PAGE_BANNER_IMAGE', 'cms_page_banner_');
define('FAQ_BACKGROUND_IMAGE', 'faq_background_image_');
define('BLOG_PHOTO', 'blog_photo_');
define('CONTENT_IMAGE', 'content_');
define('CATEGORY_IMAGE', 'category_');
define('CUSTOMER_GEN_PROBLEM', 'customer_gen_');
define('CORPORATE_PHOTO', 'corporate_photo_');
define('WORKOUT_ACTIVITY', 'workout_activity_');
define('DIETITIANS_PHOTO', 'dietitian_');
define('DIETITIANS_CHART_REPOSITORY_DOC', 'diet_chart_repository_');
define('STUDIO_AGREEMENT_DOC_', 'studio_agreement_doc_');
define('STUDIO_PHOTO_GALLERY', 'studio_gallery_');
define('VIDEO_CATEGORY_PHOTO', 'video_category_');
define('VIDEO_CATEGORY_THUMBNAIL', 'video_category_thumb_');
define('GALLERY_IMAGES', 'gallery_image_');
define("USER_CAN_COME_ANY_TIME", 1);
define("USER_CAN_COME_ONLY_SELECTED_TIME", 2);
define('FITCARD_AGENT_OFFER_PHOTO', 'fitcard_agent_offer_');


//--------------AWS placeholder for image upload start-----------------------------//
//---------------Product NOTICE Start---------------------------------//

define('MEMBERSHIP_EXPIRED_USER_NOTICE', 'Please activate your fitpass membership to freely reserve your workouts.');
define('MEMBERSHIP_ON_HOLD_USER_NOTICE', "Oops! Your memebership is on hold till");
define('STUDIO_CLOSE_USER_NOTICE', " is close for today.");
define("DIET_PLAN_EXTEND", "Customer bought diet plan");
define('NO_WORKOUT_IN_STUDIOS_LIST_DETAILS_PAGE', 'No workout for today');
define('NO_RESERVED_WORKOUT', 'No reserved workout');
define('LIST_SUCCESS_RESULT', 'Result fetch successfully');
define('FREE_WORKOUT_DIET_PLAN', 'Free workout for diet');
define('CUSTOMER_NOTIFICATION_FOR_SCHEDULE_A_CALL', 'For Schedule a call');
define('CONFIRMMESSAGE', 'Are you ure want to?');

//.................Product NOTICE End---------------------------------//
//---------------Product Staus Start---------------------------------//
define('FITSHOP_PENDING_ORDER', 1);
define('FITSHOP_SHIPPED_ORDER', 2);
define('FITSHOP_DELIVERED_ORDER', 3);
define('FITPASS_MEMBER_SHIP', 1);
define('CUSTOMER_CAN_WORKOUT_RESERVE', 1);
define('CUSTOMER_HAS_WORKOUT_RESERVED', 2);
define('WORKOUT_UNAVAILABLE', 3);
define("TEAM_TYPE_FRANCHISE", "Franchise");
define('CUSTOMER_HAS_CANCELED', 2);
define('WALLET_TRANSACTION_EARN_BY_CARD', 1);
define('CUSTOMER_HAS_ATTENDED', 3);
define('GYM_WORKOUT_ACTIVITY', 6); //workout activity come from fitpass_workouts_activity table
define('REFERRAL_DISCOUNT', 200); //Referral discount
define('FITCARD_REFERRAL_DISCOUNT', 200); //Referral discount
define('FITCARD_REFERRAL_CREDIT', 4000); //Referral discount
define('PRODUCT_TYPE_GYMMEMBERSHIP', 1); //For Gym membership
define('PRODUCT_TYPE_NUTRITIONIST', 2); //For Nutritionist
define('PRODUCT_TYPE_FITCOACH', 3); //For Fitcoach
define('PRODUCT_TYPE_FITSHOP', 4); //For Fitshop
define('PRODUCT_TYPE_FITPRIME', 5); //For FITprime
define('PRODUCT_TYPE_FITDIRECT', 6); //For FITDIRECT
define('PRODUCT_TYPE_FITCARD', 7); //For FITCARD
define('PRODUCT_TYPE_OTHER', 8); //For Other
define('PRODUCT_TYPE_FITPASS_COACH', 9); //For Other
define('PRODUCT_TYPE_FITPASS_FITFEAST', 10); //For Other
define('PRODUCT_TYPE_FITCOACH_FITFEAST', 11); //For Other
define('PRODUCT_TYPE_FITPASS_FITCOACH_FITFEAST', 12); //For Other
define('PRODUCT_TYPE_INVOICE', 13); //For Other


define('USER_TYPE_INBOUND', 'Inbound'); //Inbound User
//---------------Product Staus END---------------------------------//
//-----------------SERVER Related Start ------------------------//
//define('VENDORPATH', '/var/www/framework/vendor/');
define('awsAccessKey', 'AKIAIY7IHOCDK26FMBTA');
define('awsSecretKey', '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');
define('IMAGE_URL', 'http://api.fitpass.co.in/img/');
define('AWS_EMAIL_URL', 'https://' . AWS3DIR . '.s3.amazonaws.com/email/');
define('X_APPKEY', 'rcmroes1UWF2GIcBBQ5jghe6xpwoQ4vqDqoIIcBTbZEE6');
define('X_AUTHKEY', 'dgfJlO10QAoZzaiT8FXrF8bgBBQ5jghe7FNrd9t8D0u');
//pushwoosh details start
define('PW_AUTH', '9Q6FXnkFOB7tKFYEuGtJvqeZ44cGlEvCdnuOUPDHBYwVp2u2YBIaPzB4e6NBGwo4S01oPCbBlAp7W19jH5SH');
//define('PW_APPLICATION', '899CB-87EB6');
define('PW_DEBUG', FALSE); //
//pushwoosh details end
//define('EMAIL_HOST', 'email-smtp.eu-west-1.amazonaws.com');
//define('EMAIL_SMTP_AUTH', TRUE);
//define('EMAIL_USER_NAME', 'AKIAIOZXCG3ZZTHDLN3Q');
//define('EMAIL_PASSWORD', 'AoYCjV90bUG5wCkdKqXGr5Zk7HImBwfBYrBe4XiEHj81');



define('EMAIL_HOST', 'smtp.sendgrid.net');
define('EMAIL_SMTP_AUTH', TRUE);
define('EMAIL_USER_NAME', 'FITPASS.SMTP');
define('EMAIL_PASSWORD', 'Emailer#123');
define('EMAIL_TO_ADMIN', 'info@fitpass.co.in');
define('PRODUCT_EMAIL_FROM', 'orders@fitpass.co.in');
define('EMAIL_FROM_FOR_PARTNER', 'partners@fitpass.co.in');
define('REPLY_TO', 'info@fitpass.co.in');
define('EMAIL_FROM_FOR_CUSTOMER', 'no-reply@fitpass.co.in');
define('FITPASS_REPLY_CUSTOMER_SUPPORT', 'care@fitpass.co.in');
define('CONTACT_DETAILS', '+91-1146061468');
define('ERROR_REPORT_EMAIL', 'jnanendra.veer@fitpass.co.in');

//-------------------SERVER Related End--------------------------------------//
//----------------------------------Dynamodb table-----------------------------------//
define('Dynamodb_user_ai_chat_history', 'user_ai_chat_history');
define('Dynamodb_fitpass_product_ratings', 'fitpass_product_ratings');
define('Dynamodb_fitpass_studio_ratings', 'fitpass_studio_ratings');
define('Dynamodb_studio_log_table', 'studio_log_table');
define('Dynamodb_user_log_table', 'user_log_table');






//-------------------------------------------------------------------//
//AI QUESTION CONSTA//

define('diet_onboarding', 'diet_onboarding');
define('common_onboard', 'common_onboard');
define('diet_afterboarding', 'diet_afterboarding');
define('already_taken_diets', 'already_taken_diets');
define('schedule_a_call', 'schedule_a_call');
define('meal_not_loged', 'meal_not_loged');
define('after_meal_log', 'after_meal_log');
define('meal_time', 'meal_not_loged');
define('free_user_not_loged_meal', 'free_user_not_loged_meal');
define('fitcoach_onboarding', 'fitcoach_onboarding');
define('fitpass_users_rating', 'fitpass_users_rating');

// ABOUT FITSHOP DELIVERY

define('shipping_charge', 100);
define('minimum_order_amount', 500);

// Membership plan constant

define('PLAN_TYPE_WEEKS_INT', 1);
define('PLAN_TYPE_MONTHS_INT', 2);
define('PLAN_TYPE_WORKOUTS_INT', 3);

define('PLAN_FREE_TYPE_DAYS_INT', 1);
define('PLAN_FREE_TYPE_MONTHS_INT', 2);
define('PLAN_FREE_TYPE_WORKOUTS_INT', 3);

define('STUDIO_CATEGORY_BASIC_INT', 1);
define('STUDIO_CATEGORY_PREMIUM_INT', 2);

define('OFFER_TYPE_BIRTHDAY_INT', 1);
define('OFFER_TYPE_UBER_INT', 2);
define('OFFER_TYPE_OTHER_INT', 3);
//Wallet history
define('EARN_SOURCE_FROM_USER_REFERRAL', 1);
define('EARN_SOURCE_FROM_COUPON_CODE', 2);
define('EARN_SOURCE_FROM_FITCARD_REFERRAL', 3);
define('EARN_SOURCE_FROM_FITPASS_CREDIT', 4);

define('WALLET_TRANSACTION_CREDIT', 1);
define('WALLET_TRANSACTION_DEBIT', 2);
define('WALLET_TRANSACTION_RQUEST_FOR_CREDIT', 3);
define('WALLET_TRANSACTION_RQUEST_FOR_CONFIRM', 4);

define('RAZORPAY_ACCESS_KEY', 'rzp_live_TowRzg6WoSAFlK');
define('RAZORPAY_SECRETKEY_KEY', 'MxaCvHQtXRaeIbD6yoKyUlwP');
//Studio Activity Constent
define('STUDIO_WORKOUT_ATTENDED', 1);

define('PAYMENT_STATUS_COMPLETE', 'Complete Payment');
define('PAYMENT_STATUS_PARTIALLY', 'Partially Payment');
define('PAYMENT_STATUS_COMPLETE_INT', 1);
define('PAYMENT_STATUS_PARTIALLY_INT', 2);
define('PAYEMNT_REQUEST_FROM', "ASR Market Business Venture Pvt. Ltd.");
define('LEAD_ORDER_CONFIRM_STATUS_PENDING_INT', 1);
define('LEAD_ORDER_CONFIRM_STATUS_CONVERT_INT', 2);
define('OFFER_PRODUCT_TYPE_MEMBERSHIP_INT', 6);



