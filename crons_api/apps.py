from django.apps import AppConfig


class CronsApiConfig(AppConfig):
    name = 'crons_api'
