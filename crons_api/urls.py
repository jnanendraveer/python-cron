from django.conf.urls import url
from crons_api.controller.Membershiprenewreminder import Membershiprenewreminder
from crons_api.controller.Studiopaymentconfirmation import Studiopaymentconfirmation \
      as PaymentConfirm1
from crons_api.controller.Studiopaymentconfirmation_1 import Studiopaymentconfirmation \
      as PaymentConfirm2
from crons_api.controller.Usersreminders import Usersreminders
from crons_api.controller.Studios import Studios
from crons_api.controller.Workoutsreminder import reminder


urlpatterns = [
#__________send email to customer for membership renew_____#

      url(r'^fitpass/mail/gym-membership-ex-7', Membershiprenewreminder.fitpassmembershipexpiresinsevendays),
      url(r'^fitpass/mail/ex-24hrs', Membershiprenewreminder.fitpassmembershipexpiresin24hours),
      url(r'^fitpass/mail/ex-today', Membershiprenewreminder.fitpassmembershipexpiresintoday),
      url(r'^fitpass/mail/ex-aft-7', Membershiprenewreminder.fitpassmembershipexpiredafter7days),
      url(r'^fitpass/mail/ex-aft-30', Membershiprenewreminder.fitpassmembershipexpiredafter30days),
      url(r'^fitfeast/mail/ex-4', Membershiprenewreminder.fitfeastmembershipexpiresinfourdays),
      url(r'^fitfeast/mail/ex-24hrs', Membershiprenewreminder.fitfeastmembershipexpiresin24hours),
      url(r'^fitfeast/mail/ex-aft-7', Membershiprenewreminder.fitfeastmembershipexpiredafter7days),
      url(r'^fitfeast/mail/ex-aft-30', Membershiprenewreminder.fitfeastmembershipexpiredafter30days),
      url(r'^fitpass/mail/pre-reminder', Membershiprenewreminder.coachmembershipreminder),

#_________________________studio_payment_confirmation____________________________#

      url(r'^studio-payment/mail/', PaymentConfirm1.actionIndex),
      # url(r'^studio-payment-report', PaymentConfirmController1.actionGeneratestudiopaymentreports),
      url(r'^studio-payment1/mail/', PaymentConfirm2.actionIndex),
      # url(r'^studio-payment1/diet-monthly/', PaymentConfirmController2.actionDietmonthly),

#______________________________users____________________________________#

      url(r'^user-reminder/', Usersreminders.actionIndex),


# ________________________studio_workout_reminder____________________________________#

      # url(r'^studio/workout-reminder', Studios.fiveourbefore),
      url(r'^studio/spacefic-studioworkout-reminder', Studios.specificstudioreminder),
      url(r'^studio/workout-reminder',reminder),


]