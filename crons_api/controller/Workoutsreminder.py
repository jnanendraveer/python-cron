import sys
sys.path.append('/var/www/html/crons_project')
from django.http import HttpResponse
import mysql.connector
from mysql.connector import Error
from mysql.connector import errorcode
import json


def reminder(request):
    user_name = request.POST['user_name']
    fitpass_id = request.POST['fitpass_id']
    is_active = request.POST['is_active']
    is_cancel = request.POST['is_cancel']
    remarks = request.POST['remarks']
    studio_id = request.POST['studio_id']
    workout_name = request.POST['workout_name']
    workout_time = request.POST['workout_time']
    user_id = request.POST['user_id']
    user_status = request.POST['user_status']
    studio_status = request.POST['studio_status']
    create_time = request.POST['create_time']
    studio_name = request.POST['studio_name']
    studio_address = request.POST['studio_address']
    user_mobile = request.POST['user_mobile']
    studio_mobile = request.POST['studio_mobile']
    user_schedule_id = request.POST['user_schedule_id']
    urc = request.POST['urc']
    MysqlConnect = mysql.connector.connect(host='localhost', database='live_data_fitpass_2015', user='root', password='Fitpass@123')
    cursor = MysqlConnect.cursor()
    try:
        query = " INSERT INTO live_data_fitpass_2015.fitpass_workouts_reminder(studio_id,workout_name,workout_time,user_id,"\
                " create_time, studio_name, studio_address, urc, user_mobile, studio_mobile, user_schedule_id, user_name, fitpass_id, is_active,is_cancel, remarks) " \
                " VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"

        args = (studio_id, workout_name, workout_time, user_id, create_time, studio_name, studio_address, urc, user_mobile,
                studio_mobile, user_schedule_id, user_name, fitpass_id, is_active, is_cancel, remarks)
        cursor.execute(query, args)
        d = cursor.lastrowid
        MysqlConnect.commit()
        return HttpResponse("Data inserting successfully")
    except mysql.connector.Error as error:
        MysqlConnect.rollback()  # rollback if any exception occured
        return HttpResponse("Failed inserting data ".format(error))