import sys
sys.path.append('/var/www/html/crons_project')
from datetime import datetime,timedelta
from crons_api.connection.MySqlConnection import MySqlConnection
from crons_api.constant.FITPASSConfig import FITPASSConfig
from crons_api.constant.FITPASSApikey import FITPASSApikey
from pyfcm import FCMNotification
import sendgrid
from crons_api.connection.SendGridConnection import SendGrid


class Studios:

    def oneourbefore(self):
        data1 = datetime.now() + timedelta(hours=1, minutes=0, seconds=0)
        data2 = datetime.now() + timedelta(hours=1, minutes=10, seconds=0)
        startTime = data1.strftime('%Y-%m-%d %H:%M:%S')
        endTime = data2.strftime('%Y-%m-%d %H:%M:%S')
        print(startTime,endTime)
        return Studios.studiosWorkoutReminder(startTime, endTime, '60')

    def fiveourbefore(self):
        data1 = datetime.now() + timedelta(hours=5, minutes=0, seconds=0)
        data2 = datetime.now() + timedelta(hours=8, minutes=0, seconds=0)
        startTime = str(data1.strftime('%Y-%m-%d %H:%M:%S'))
        endTime = str(data2.strftime('%Y-%m-%d %H:%M:%S'))
        return Studios.studiosWorkoutReminder(startTime, endTime, '5')

    def twoourbefore(self):
        data1 = datetime.now() + timedelta(hours=2, minutes=0, seconds=0)
        data2 = datetime.now() + timedelta(hours=2, minutes=5, seconds=0)
        startTime = data1.strftime('%Y-%m-%d %H:%M:%S')
        endTime = data2.strftime('%Y-%m-%d %H:%M:%S')
        return Studios.studiosWorkoutReminder(startTime, endTime, '120')

    def halfanourbefore(self):
        data1 = datetime.now() - timedelta(weeks=200)
        data2 = datetime.now() - timedelta(weeks=150)
        startTime = data1.strftime('%Y-%m-%d %H:%M:%S')
        endTime = data2.strftime('%Y-%m-%d %H:%M:%S')
        return Studios.studiosWorkoutReminder(startTime, endTime, '30')

    def printCustomerData(customer, i):
        data = """<tr style='padding-top:10px'>
        <td width='10%' style='font-weight:500;color:#a2a1a1; font-size:12px; padding:5px 0;border-bottom-style:inset;border-bottom-width:thin'>""" + str(i) + """</td>
        <td width='40%' style='font-weight:500;color:#222222;font-size:12px; border-bottom-style:inset;border-bottom-width:thin;text-align:left'>""" + str(customer['user_membership_id']) + """ </td>
        <td width='50%' style='font-weight:500;color:#222222;font-size:12px; border-bottom-style:inset;border-bottom-width:thin;text-align:left'>""" + str(customer['user_name']) + """ </td>
           </tr>"""
        return data

    def studiosWorkoutReminder(startTime, endTime, time):
        customerData = ''
        where = 'wr. workout_cancel_status="No" AND '
        if time != '5':
            where = " wr.studio_status='No' AND studio.studio_reminder=" + time + " AND wr.workout_time BETWEEN 2019-01-31 12:18:20 AND 2019-01-31 12:28:20"

        else:
            where = " wr.studio_status='No' AND wr.workout_time BETWEEN 2019-01-31 12:18:20 AND 2019-01-31 12:28:20"
        MySqlConnection.cursor.execute(" SELECT studio.device_token,studio.device_type,studio.team_zone_name,wr.workouts_reminder_id, wr.workout_name,wr.workout_time,studio.phone_number as studio_number, studio.email_address,studio.studio_logo,studio.fitpass_poc,studio.group_email_address,studio.alternate_email_address,studio.studio_id,studio.studio_name FROM  fitpass_workouts_reminder AS wr INNER JOIN fitpass_studios as studio ON (wr.studio_id=studio.studio_id) where  " + where + " group by studio.studio_id")
        studios = MySqlConnection.cursor.fetchall()

        for studio in studios:
            MySqlConnection.cursor.execute(" SELECT fu.user_name,fu.user_membership_id FROM  fitpass_workouts_reminder AS wr INNER JOIN fitpass_users AS fu ON ( fu.user_id = wr.user_id ) INNER JOIN fitpass_studios as  studio ON (wr.studio_id=studio.studio_id) where  " + where + "AND studio.studio_id = %s group by fu.user_id ", (studio['studio_id'],))
            getCustomers = MySqlConnection.cursor.fetchall()
            i = 1
            for customer in getCustomers:
                customerData = Studios.printCustomerData(customer, i)
                i = i+1
            if FITPASSConfig.getCustomerSupportEmailAddress(studio['team_zone_name'])['partner_support'] != FITPASSConfig.EMAIL_FROM_FOR_PARTNER:
                reply_email_address = FITPASSConfig.getCustomerSupportEmailAddress(studio['team_zone_name'])['partner_support'] + ',' + FITPASSConfig.EMAIL_FROM_FOR_PARTNER

            else:
                reply_email_address = FITPASSConfig.EMAIL_FROM_FOR_PARTNER

            with open('/var/www/html/crons_project/templete/mail/studio_workout_reminder.html', 'r') as f:
                    html = f.read()
                    mailBody = html.replace("#CUSTOMER_DATA#", customerData)
                    message = sendgrid.Mail()
                    message.add_to(FITPASSConfig.EMAIL_TO.islower())
                    message.set_from(FITPASSConfig.EMAIL_FROM)
                    message.set_subject( studio['team_zone_name'] + ' | ' + FITPASSConfig.STUDIO_WORKOUT_RESERVATION_CONFIRMATION + studio['studio_name'] + '  |  ' + studio['workout_name'] + '  |  ' + datetime.strptime(str(studio['workout_time']), '%Y-%m-%d %H:%M:%S').strftime('%H:%M'))
                    message.set_html(mailBody)
                    message.add_bcc(FITPASSConfig.EMAIL_TO)
                    message.add_cc(FITPASSConfig.EMAIL_TO)
                    message.set_replyto(FITPASSConfig.EMAIL_REPLY_TO)
                    emailsend = SendGrid.send(message)
            studioSMS = "attendee(s) has been confirmed for your " + studio['workout_name'] + " class from  today"

            if studio['device_type'] == "Android":
                Studios.sendAndroidpartnernotification(studio['device_token'], studioSMS)
            else:
                print('nothing type')

            if emailsend:
                MySqlConnection.cursor.execute(" update fitpass_workouts_reminder SET studio_status='Yes' where studio_status='No' and workouts_reminder_id = %s ", (studio['workouts_reminder_id']))
                d=MySqlConnection.cursor.rowcount()
                print(d)
            customerData = ''

    def specificstudioreminder(self):
        data1 = datetime.now() + timedelta(minutes=5, seconds=0)
        data2 = datetime.now() + timedelta(minutes=7, seconds=0)
        startTime = data1.strftime('%Y-%m-%d %H:%M:%S')
        endTime = data2.strftime('%Y-%m-%d %H:%M:%S')
        customerData = ''
        # where = 'wr. workout_cancel_status="No" AND '
        where = " studio.studio_id = 1236 AND wr.studio_status='No' AND wr.workout_time BETWEEN '2015-06-01 07:04:44' AND '2018-06-01 07:04:44' "
        MySqlConnection.cursor.execute(" SELECT studio.team_zone_name, wr.workouts_reminder_id, wr.workout_name,wr.workout_time,studio.phone_number as studio_number, studio.email_address,studio.fitpass_poc,studio.studio_logo,studio.group_email_address,studio.alternate_email_address,studio.studio_id,studio.studio_name FROM  fitpass_workouts_reminder AS wr INNER JOIN fitpass_studios as studio ON (wr.studio_id=studio.studio_id) where """ + where + """ group by studio.studio_id """)
        studios = MySqlConnection.cursor.fetchall()

        for studio in studios:
            MySqlConnection.cursor.execute(" SELECT fu.user_name,fu.user_membership_id FROM  fitpass_workouts_reminder AS wr INNER JOIN fitpass_users AS fu ON ( fu.user_id = wr.user_id ) INNER JOIN fitpass_studios as  studio ON (wr.studio_id=studio.studio_id) where  " + where + " AND studio.studio_id = %s group by fu.user_id ", (studio['studio_id'],))
            getCustomers = MySqlConnection.cursor.fetchall()
            i = 1
            for customer in getCustomers:
                customerData = Studios.printCustomerData(customer, i)
                i = +1

            if FITPASSConfig.getCustomerSupportEmailAddress(studio['team_zone_name'])['partner_support'] != FITPASSConfig.EMAIL_FROM_FOR_PARTNER:
                reply_email_address = FITPASSConfig.getCustomerSupportEmailAddress(studio['team_zone_name'])['partner_support'] + ',' + FITPASSConfig.EMAIL_FROM_FOR_PARTNER
            else:
                reply_email_address = FITPASSConfig.EMAIL_FROM_FOR_PARTNER

            with open('/var/www/html/crons_project/templete/mail/studio_workout_reminder.html', 'r') as f:
                html = f.read()
                mailBody = html.replace("#CUSTOMER_DATA#", customerData)
                message = sendgrid.Mail()
                message.add_to(FITPASSConfig.EMAIL_TO.islower())
                message.set_from(FITPASSConfig.EMAIL_FROM)
                message.set_subject( studio['team_zone_name'] + ' | ' + FITPASSConfig.STUDIO_WORKOUT_RESERVATION_CONFIRMATION + studio['studio_name'] + '  |  ' + studio['workout_name'] + '  |  ' + datetime.strptime(str(studio['workout_time']), '%Y-%m-%d %H:%M:%S').strftime('%H:%M'))
                message.set_html(mailBody)
                message.add_bcc(FITPASSConfig.EMAIL_TO.islower())
                message.add_cc(FITPASSConfig.EMAIL_TO.islower())
                message.set_replyto(FITPASSConfig.EMAIL_REPLY_TO)
                emailsend = SendGrid.send(message)
                print(emailsend)

            if emailsend:
                MySqlConnection.cursor.execute(" update fitpass_workouts_reminder SET studio_status='Yes' where studio_status='No' and workouts_reminder_id = %s " , (studio['workouts_reminder_id'],))
            customerData = ''
            exit()

    def sendAndroidpartnernotification(studio,studioSMS):
        push_service = FCMNotification(api_key=FITPASSApikey.AuthCode, proxy_dict=FITPASSApikey.proxy_dict)
        registration_id = FITPASSApikey.PartnerDeviceId
        message_title = "update"
        message_body = studioSMS
        result = push_service.notify_single_device(registration_id=registration_id, message_title=message_title, message_body=message_body)
        print(result)

    # def sendiOSAppnotification(notify_id, studioSMS) :


if __name__ == '__main__':
    if sys.argv[1] == 'oneourbefore':
        Studios.oneourbefore(str)

    elif sys.argv[1] == 'fievebefore':
        Studios.fiveourbefore(str)

    elif sys.argv[1] == 'twoourbefore':
        Studios.twoourbefore(str)

    elif sys.argv[1] == 'halfanourbefore':
        Studios.halfanourbefore(str)

    elif sys.argv[1] == 'specificstudioreminder':
        Studios.specificstudioreminder(str)