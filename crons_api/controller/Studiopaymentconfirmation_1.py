import sys
sys.path.append('/var/www/html/crons_project')
from crons_api.connection.MySqlConnection import MySqlConnection
from crons_api.constant.FITPASSConfig import FITPASSConfig
import datetime as DT
import sendgrid
from crons_api.connection.SendGridConnection import SendGrid


class Studiopaymentconfirmation:

    def actionIndex(self):
        next7daysDate = '2018-09-01 00:00:10'
        # $userDetails = \Yii::$app->db->createCommand('select * from fitpass_studios_payment where  email_send="No" AND payment_of_month = "2017-08-01"');
        MySqlConnection.cursor.execute("""select * from fitpass_studios_payment as payment 
                          inner join fitpass_studios as studio on studio.studio_id = payment.studio_id 
                          where email_send='No' AND payment.create_time between %s AND %s """, (next7daysDate, '2018-9-13 00:06:10'))
        userArray = MySqlConnection.cursor.fetchall()
        print(userArray)
        if userArray:
            for userDetails in userArray:
                arrayData = {
                    'payment_date': 'None' if str(userDetails['payment_date']) == 'None' else DT.datetime.strptime(userDetails['payment_date'], "%Y-%m-%d").strftime("%d-%m-%Y"),
                    'payment_of_month': 'None' if str(userDetails['payment_of_month']) == 'None' else DT.datetime.strptime(str(userDetails['payment_of_month']), "%Y-%m-%d").strftime("%B-%Y"),
                }

                with open('/var/www/html/crons_project/templete/mail/studio_payment_confirmation.html', 'r') as f:
                     html = f.read()
                     mailBody = html.replace("#PAYMENT_AMOUNT#", str(userDetails['payment_amount'])).\
                                     replace("#PAYMENT_OF_MONTH#", arrayData["payment_of_month"])
                     message = sendgrid.Mail()
                     message.add_to(FITPASSConfig.EMAIL_TO.islower())
                     message.set_from(FITPASSConfig.EMAIL_FROM)
                     message.set_subject( FITPASSConfig.STUDIO_PAYMENT_CONFIRMATION + '[' + DT.datetime.strptime(str(userDetails['payment_of_month']), "%Y-%m-%d").strftime("%b,%Y") + ']')
                     message.set_html(mailBody)
                     message.add_bcc(FITPASSConfig.EMAIL_TO.islower())
                     message.add_cc(FITPASSConfig.EMAIL_TO.islower())
                     message.set_replyto(FITPASSConfig.EMAIL_REPLY_TO)
                     response = SendGrid.send(message)
                     print(response)

                if response:
                    MySqlConnection.cursor.execute(" UPDATE fitpass_studios_payment SET email_send='Yes' WHERE studio_payment_id= %s", (userDetails['studio_payment_id'], ))

    # def actionDietmonthly(self):
    #     today = '2018-09-01'
    #     next3daysDate = '2018-09-13'
    #     MySqlConnection.cursor.execute(
    #         """select user_membership_id,user_id,user_email,user_name,diet_end_of_cycle
    #            from fitpass_users where diet_end_of_cycle
    #            between %s AND %s """, (today, next3daysDate)
    #     )
    #     userArray = MySqlConnection.cursor.fetchall()
    #     print(userArray)
    #     if userArray:
    #         for userDetails in userArray:
    #             arrayData = {
    #                 'emailFrom': FITPASSConfig.EMAIL_FROM,
    #                 'user_name': userDetails['user_name'],
    #                 'diet_end_of_cycle': '' if str(userDetails['diet_end_of_cycle']) == 'Null' else DT.datetime.strptime(userDetails['diet_end_of_cycle'],'%Y-%m-%d').strftime('%a,%d %b'),
    #                 'emailTo': userDetails['user_email'],
    #                 'membership_id': userDetails['user_membership_id'],
    #                 'subject': 'Diet Membership Expiry'}
    #             print(arrayData)
    #
    #             with open('templete/mail/studio_payment_confirmation.html', 'r') as f:
    #                 html = f.read()
    #                 find = ['#USER_NAME#'],
    #                 replac = [arrayData['user_name']]
    #                 newhtmlData = html.replace("#USER_NAME#", arrayData['user_name'])
    #                 message = sendgrid.Mail()
    #                 message.add_to(FITPASSConfig.EMAIL_TO.islower())
    #                 message.set_from("rohit.rawat@fitpass.co.in")
    #                 message.set_subject("invoice")
    #                 message.set_html(newhtmlData)
    #                 # message.add_bcc(FITPASSConfig.EMAIL_TO.islower())
    #                 # message.add_cc(FITPASSConfig.EMAIL_TO.islower())
    #                 response = sg.send(message)