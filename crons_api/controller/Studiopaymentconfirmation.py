import sys
sys.path.append('/var/www/html/crons_project')
from crons_api.connection.MySqlConnection import MySqlConnection
from crons_api.constant.FITPASSConfig import FITPASSConfig
import datetime
import sendgrid
from crons_api.connection.SendGridConnection import SendGrid


class Studiopaymentconfirmation():

    def actionIndex(self):
        next7daysDate = '2018-12-01 00:00:23'
        # MySqlConnection.cursor.execute("select * from fitpass_studios_payment where  email_send='No' AND payment_of_month = '2017-08-01'")
        MySqlConnection.cursor.execute(
            " select * from fitpass_studios_payment as payment "
            " inner join fitpass_studios as studio on studio.studio_id = payment.studio_id "
            " where email_send='No' AND payment.create_time between %s AND %s ", (next7daysDate, '2018-12-04 00:00:23'))

        userarray = MySqlConnection.cursor.fetchall()


        # return HttpResponse(json.dump(userarray))
        if userarray is not None:
            for userDetails in userarray:
                print(userDetails)

                arrayData = {
                    'payment_date': 'None' if str(userDetails['payment_date']) == 'None' else datetime.datetime.strptime("%Y-%m-%d", str(userDetails['payment_date'])).strftime("%d-%m-%Y"),
                    'payment_of_month': 'None' if str(userDetails['payment_of_month']) == 'None' else datetime.datetime.strptime( str(userDetails['payment_of_month']),"%Y-%m-%d").strftime("%B-%Y"),
                }
                print(arrayData)
                with open('templete/mail/studio_payment_confirmation.html', 'r') as f:
                    html = f.read()
                    mailBody = html.replace("#PAYMENT_AMOUNT#", str(userDetails['payment_amount'])).\
                                       replace("#PAYMENT_OF_MONTH#", arrayData["payment_of_month"]).\
                                       replace("#PAYMENT_DATE#", arrayData["payment_date"]).\
                                       replace("#NUMBER_OF_ATTENDED_WORKOUT#", userDetails["number_of_attended_workout"]).\
                                       replace("#BANK_UTR_NUMBER#", userDetails["bank_utr_number"])
                    message = sendgrid.Mail()
                    message.add_to(FITPASSConfig.EMAIL_TO.islower())
                    message.set_from(FITPASSConfig.EMAIL_FROM)
                    message.set_subject(FITPASSConfig.STUDIO_PAYMENT_CONFIRMATION + '[' + datetime.datetime.strptime(str(userDetails['payment_of_month']),"%Y-%m-%d").strftime("%b,%Y") +']')
                    message.set_html(mailBody)
                    message.add_bcc(FITPASSConfig.EMAIL_TO.islower())
                    message.add_cc(FITPASSConfig.EMAIL_TO.islower())
                    message.set_replyto(FITPASSConfig.EMAIL_REPLY_TO)
                    response = SendGrid.send(message)

                if response:
                    MySqlConnection.cursor.execute(
                            "UPDATE fitpass_studios_payment SET email_send='Yes' WHERE studio_payment_id="+str(userDetails['studio_payment_id']))
                    d=MySqlConnection.cursor.rowcount()
                    print(d)

    # def actionGeneratestudiopaymentreports(self):
    #     today = datetime.date.today()
    #     first = today.replace(day=1)
    #     startDate=today - DT.timedelta(months = 1)
    #     endDate = first - datetime.timedelta(days = 1)
    #
    #     MySqlConnection.cursor.execute(
    #         """select urs.workout_date,
    #                     urs.studio_id,
    #                     urs.studio_zone,
    #                     fs.studio_name, fs.number_of_visit,
    #                     fs.per_visit_price,fsbd.bank_name,fsbd.account_holder_name,
    #                     fsbd.account_number,fsbd.ifsc_code,fsbd.micr_code,fsbd.branch_name,
    #                     fsbd.corporate_name,fsbd.gst_number,
    #                     COUNT(CASE WHEN urs.workout_status=3 THEN urs.workout_status ELSE NULL END) AS attended_workouts,
    #                     sum(CASE WHEN urs.workout_status=3 THEN urs.studio_payment ELSE NULL END) AS studioPayments,
    #                     COUNT(CASE WHEN (urs.workout_status=1 OR urs.workout_status=3) THEN urs.workout_status ELSE NULL END) AS reserved_workouts
    #                     from fitpass_user_reserved_schedules as urs
    #                     left join fitpass_studios as fs ON (urs.studio_id=fs.studio_id)
    #                     LEFT JOIN fitpass_studio_bank_details AS fsbd ON fsbd.studio_id = urs.studio_id
    #                     where date_format(urs.workout_date,"%Y-%m-%d") between """ + str(startDate) +""" AND  """+str(endDate) +""" AND  urs.workout_status!=2  group by urs.studio_zone,urs.studio_id having studioPayments>0""")
    #     studioPaymentArray = MySqlConnection.cursor.fetchall()
    #     print(studioPaymentArray)

        # if studioPaymentArray :
        #     for studiosPayment in studioPaymentArray:
        #         oldstudioPayment=where('date_format(payment_of_month,"%Y-%m-%d") between "'+startDate +'" AND "' +endDate + '" AND studio_id='+studioPayment['studio_id'])
        #         bankDetails = empty(oldstudioPayment) ?
        #                 ["bank_name" : isset($studioPayment["bank_name"]) ? $studioPayment["bank_name"] : '',
        #             "account_holder_name" : isset($studioPayment['account_holder_name']) ? $studioPayment['account_holder_name'] : '',
        #             "account_number" : isset($studioPayment['account_number']) ? $studioPayment['account_number'] : '',
        #             "ifsc_code" : isset($studioPayment['ifsc_code']) ? $studioPayment['ifsc_code'] : '',
        #             "branch_name" : isset($studioPayment['branch_name']) ? $studioPayment['branch_name'] : '',
        #             "gst_number" : isset($studioPayment['gst_number']) ? $studioPayment['gst_number'] : '',
        #             "corporate_name" : isset($studioPayment['corporate_name']) ? $studioPayment['corporate_name'] : '',] : json_decode($oldstudioPayment->bank_details, true);
        #         $studiosPaymentModel = !empty($oldstudioPayment) ? $oldstudioPayment : new \app\models\StudiosPayment;
        #         $studiosPaymentModel->payment_of_month = date("Y-m-d", strtotime($studioPayment['workout_date']));
        #         $studiosPaymentModel->bank_utr_number = "";
        #         $studiosPaymentModel->studio_id = $studioPayment['studio_id'];
        #         $studiosPaymentModel->studio_name = $studioPayment['studio_name'];
        #         $studiosPaymentModel->pay_amount = $studioPayment['studioPayments'];
        #         $studiosPaymentModel->paid_amount = !empty($studioPayment['paid_amount']) ? $studioPayment['paid_amount'] : '';
        #         $studiosPaymentModel->create_time = dateTime();
        #         $studiosPaymentModel->payment_status = !empty($oldstudioPayment) ? $oldstudioPayment->payment_status : "Pending";
        #         $studiosPaymentModel->created_by = "team-fitpass";
        #         $studiosPaymentModel->reserve_workouts = $studioPayment['reserved_workouts'];
        #         $studiosPaymentModel->attended_workouts = $studioPayment['attended_workouts'];
        #         $studiosPaymentModel->per_workout_price = $studioPayment['per_visit_price'];
        #         $studiosPaymentModel->number_of_workout = $studioPayment['number_of_visit'];
        #         $studiosPaymentModel->bank_details = json_encode($bankDetails);
        #         $studiosPaymentModel->team_zone_name = $studioPayment['studio_zone'];
        #         $studiosPaymentModel->save(false);
