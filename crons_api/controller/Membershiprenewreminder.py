import sys
sys.path.append('/var/www/html/crons_project')
import datetime as DT
from crons_api.connection.MySqlConnection import MySqlConnection
from crons_api.constant.FITPASSConfig import FITPASSConfig
from crons_api.connection.SendGridConnection import SendGrid
import sendgrid


class Membershiprenewreminder:

##customer membership reminder before 7 days

    def fitpassmembershipexpiresinsevendays(self):
        today = DT.date.today()
        next7daysDate = today + DT.timedelta(weeks=1)
        MySqlConnection.cursor.execute(" select team_zone_name,user_id,user_membership_id,user_id,user_email,user_name,gym_end_of_cycle from fitpass_users where gym_end_of_cycle between %s and %s ", (today, next7daysDate))
        userarray = MySqlConnection.cursor.fetchall()
        if userarray is not None:

            for userDetails in userarray:
                MySqlConnection.cursor.execute(" update fitpass_users  SET user_status='Renew' WHERE user_id= %s ", (userDetails["user_id"],))
                # print(FITPASSConfig.getCustomerSupportEmailAddress(userDetails['team_zone_name']))

                if FITPASSConfig.getCustomerSupportEmailAddress(userDetails['team_zone_name'])['customer_support'] != FITPASSConfig.FITPASS_REPLY_CUSTOMER_SUPPORT:
                    reply_email_address = FITPASSConfig.getCustomerSupportEmailAddress(userDetails['team_zone_name'])['customer_support'] +',' + FITPASSConfig.FITPASS_REPLY_CUSTOMER_SUPPORT
                else:
                    reply_email_address = FITPASSConfig.FITPASS_REPLY_CUSTOMER_SUPPORT
                    # print(FITPASSConfig.getCustomerSupportEmailAddress(userDetails['team_zone_name'])['customer_support'])

                with open('/var/www/html/crons_project/templete/mail/email-to-customer-gym-membership-renewal-reminder.html', 'r') as f:
                    html = f.read()
                    mailBody = html.replace("#USER_NAME#", userDetails['user_name']).replace("#MAIL_BODY#", FITPASSConfig.FITPASS_7DAY_BEFORE_REMINDER_BODY)
                    message = sendgrid.Mail()
                    message.add_to(FITPASSConfig.EMAIL_TO.islower())
                    message.set_from(FITPASSConfig.EMAIL_FROM)
                    message.set_subject(userDetails['team_zone_name'] + "| FITPASS expires in 7 days")
                    message.set_html(mailBody)
                    message.add_bcc(FITPASSConfig.EMAIL_TO)
                    message.add_cc(FITPASSConfig.EMAIL_TO)
                    message.set_replyto(FITPASSConfig.EMAIL_REPLY_TO)
                    response = SendGrid.send(message)
                    # return HttpResponse(response, content_type='application/json')
                    exit()

    def fitpassmembershipexpiresin24hours(self):
        today = DT.date.today()
        next7daysDate = today + DT.timedelta(days=+1)
        MySqlConnection.cursor.execute(" select team_zone_name,user_id,user_membership_id,user_id,user_email,user_name,gym_end_of_cycle from fitpass_users where gym_end_of_cycle between %s and %s ", (today, next7daysDate))
        userarray = MySqlConnection.cursor.fetchall()
        if userarray is not None:
            for userDetails in userarray:
                if FITPASSConfig.getCustomerSupportEmailAddress(userDetails['team_zone_name'])['customer_support'] != FITPASSConfig.FITPASS_REPLY_CUSTOMER_SUPPORT:
                    reply_email_address = FITPASSConfig.getCustomerSupportEmailAddress(userDetails['team_zone_name'])[
                                              'customer_support'] + ',' + FITPASSConfig.FITPASS_REPLY_CUSTOMER_SUPPORT
                else:
                    reply_email_address = FITPASSConfig.FITPASS_REPLY_CUSTOMER_SUPPORT

                with open('/var/www/html/crons_project/templete/mail/email-to-customer-gym-membership-renewal-reminder.html', 'r') as f:
                    html = f.read()
                    mailBody = html.replace("#USER_NAME#", userDetails['user_name']).replace("#MAIL_BODY#", FITPASSConfig.FITPASS_1DAY_BEFORE_REMINDER_BODY)
                    message = sendgrid.Mail()
                    message.add_to(FITPASSConfig.EMAIL_TO.islower())
                    message.set_from(FITPASSConfig.EMAIL_FROM)
                    message.set_subject(userDetails['team_zone_name'] + "| FITPASS subscription expires in 24 hours")
                    message.set_html(mailBody)
                    message.add_bcc(FITPASSConfig.EMAIL_TO)
                    message.add_cc(FITPASSConfig.EMAIL_TO)
                    message.set_replyto(FITPASSConfig.EMAIL_REPLY_TO)
                    response = SendGrid.send(message)
                exit()

    def fitpassmembershipexpiresintoday(self):
        today = DT.date.today()
        next7daysDate = today + DT.timedelta(days=+0)
        MySqlConnection.cursor.execute("select team_zone_name,user_id,user_membership_id,user_id,user_email,user_name,gym_end_of_cycle from fitpass_users where  gym_end_of_cycle  between %s and %s ", (today, next7daysDate))
        userarray = MySqlConnection.cursor.fetchall()
        if userarray is not None:

            for userDetails in userarray:
                if FITPASSConfig.getCustomerSupportEmailAddress(userDetails['team_zone_name'])['customer_support'] != FITPASSConfig.FITPASS_REPLY_CUSTOMER_SUPPORT:
                    reply_email_address = FITPASSConfig.getCustomerSupportEmailAddress(userDetails['team_zone_name'])[
                                              'customer_support'] + ',' + FITPASSConfig.FITPASS_REPLY_CUSTOMER_SUPPORT
                else:
                    reply_email_address = FITPASSConfig.FITPASS_REPLY_CUSTOMER_SUPPORT

                with open('/var/www/html/crons_project/templete/mail/email-to-customer-gym-membership-renewal-reminder.html', 'r') as f:
                    html = f.read()
                    mailBody = html.replace("#USER_NAME#", userDetails['user_name']).replace("#MAIL_BODY#", FITPASSConfig.FITPASS_TODAY_REMINDER_BODY)
                    message = sendgrid.Mail()
                    message.add_to(FITPASSConfig.EMAIL_TO.islower())
                    message.set_from(FITPASSConfig.EMAIL_FROM)
                    message.set_subject(userDetails['team_zone_name'] + "|  FITPASS membership is pending for renewal")
                    message.set_html(mailBody)
                    message.add_bcc(FITPASSConfig.EMAIL_TO)
                    message.add_cc(FITPASSConfig.EMAIL_TO)
                    message.set_replyto(FITPASSConfig.EMAIL_REPLY_TO)
                    response = SendGrid.send(message)
                    exit()

    def fitpassmembershipexpiredafter7days(self):
        today = DT.date.today()
        next7daysDate = today + DT.timedelta(days=-7)
        MySqlConnection.cursor.execute("select team_zone_name,user_id,user_membership_id,user_id,user_email,user_name,gym_end_of_cycle from fitpass_users where  gym_end_of_cycle  between %s and %s ", (next7daysDate,today))
        userarray = MySqlConnection.cursor.fetchall()
        if userarray is not None:
            for userDetails in userarray:
                if FITPASSConfig.getCustomerSupportEmailAddress(userDetails['team_zone_name'])['customer_support'] != FITPASSConfig.FITPASS_REPLY_CUSTOMER_SUPPORT:
                    reply_email_address = FITPASSConfig.getCustomerSupportEmailAddress(userDetails['team_zone_name'])['customer_support'] + ',' + FITPASSConfig.FITPASS_REPLY_CUSTOMER_SUPPORT
                else:
                    reply_email_address = FITPASSConfig.FITPASS_REPLY_CUSTOMER_SUPPORT

                with open('/var/www/html/crons_project/templete/mail/email-to-customer-gym-membership-renewal-reminder.html', 'r') as f:
                    html = f.read()
                    mailBody = html.replace("#USER_NAME#", userDetails['user_name']).replace("#MAIL_BODY#", FITPASSConfig.FITPASS_7DAY_AFTER_REMINDER_BODY)
                    message = sendgrid.Mail()
                    message.add_to(FITPASSConfig.EMAIL_TO.islower())
                    message.set_from(FITPASSConfig.EMAIL_FROM)
                    message.set_subject(userDetails['team_zone_name'] + "|  FITPASS membership expired")
                    message.set_html(mailBody)
                    message.add_bcc(FITPASSConfig.EMAIL_TO)
                    message.add_cc(FITPASSConfig.EMAIL_TO)
                    message.set_replyto(FITPASSConfig.EMAIL_REPLY_TO)
                    response = SendGrid.send(message)
                    exit()

    def fitpassmembershipexpiredafter30days(self):
        today = DT.date.today()
        next7daysDate = today + DT.timedelta(days=-30)
        MySqlConnection.cursor.execute("select team_zone_name,user_id,user_membership_id,user_id,user_email,user_name,gym_end_of_cycle from fitpass_users where  gym_end_of_cycle  between %s and %s ", (next7daysDate,today ))
        userarray = MySqlConnection.cursor.fetchall()
        if userarray is not None:
            for userDetails in userarray:
                if FITPASSConfig.getCustomerSupportEmailAddress(userDetails['team_zone_name'])['customer_support'] != FITPASSConfig.FITPASS_REPLY_CUSTOMER_SUPPORT:
                    reply_email_address = FITPASSConfig.getCustomerSupportEmailAddress(userDetails['team_zone_name'])[
                                              'customer_support'] + ',' + FITPASSConfig.FITPASS_REPLY_CUSTOMER_SUPPORT
                else:
                    reply_email_address = FITPASSConfig.FITPASS_REPLY_CUSTOMER_SUPPORT
                with open('/var/www/html/crons_project/templete/mail/email-to-customer-gym-membership-renewal-reminder-after-30days.html', 'r') as f:
                    html = f.read()
                    mailBody = html.replace("#USER_NAME#", userDetails['user_name']).replace("#MAIL_BODY#", FITPASSConfig.FITPASS_30DAY_AFTER_REMINDER_BODY)
                    message = sendgrid.Mail()
                    message.add_to(FITPASSConfig.EMAIL_TO.islower())
                    message.set_from(FITPASSConfig.EMAIL_FROM)
                    message.set_subject(userDetails['team_zone_name'] + "|  FITPASS membership expired")
                    message.set_html(mailBody)
                    message.add_bcc(FITPASSConfig.EMAIL_TO)
                    message.add_cc(FITPASSConfig.EMAIL_TO)
                    message.set_replyto(FITPASSConfig.EMAIL_REPLY_TO)
                    response = SendGrid.send(message)
                    exit()

    def fitfeastmembershipexpiresinfourdays(self):
        today = DT.date.today()
        next7daysDate = today + DT.timedelta(days=+4)
        MySqlConnection.cursor.execute("select team_zone_name,user_id,user_membership_id,user_id,user_email,user_name,diet_end_of_cycle from fitpass_users where  diet_end_of_cycle  between %s and %s ", (today, next7daysDate))
        userarray = MySqlConnection.cursor.fetchall()
        if userarray is not None:
            for userDetails in userarray:
                if FITPASSConfig.getCustomerSupportEmailAddress(userDetails['team_zone_name'])['customer_support'] != FITPASSConfig.FITPASS_REPLY_CUSTOMER_SUPPORT:
                    reply_email_address = FITPASSConfig.getCustomerSupportEmailAddress(userDetails['team_zone_name'])[
                                              'customer_support'] + ',' + FITPASSConfig.FITPASS_REPLY_CUSTOMER_SUPPORT
                else:
                    reply_email_address = FITPASSConfig.FITPASS_REPLY_CUSTOMER_SUPPORT

                with open('/var/www/html/crons_project/templete/mail/email-to-customer-fitfeast-membership-renewal-reminder-after-30days.html', 'r') as f:
                      html = f.read()
                      mailBody = html.replace("#USER_NAME#", userDetails['user_name']).replace("#MAIL_BODY#",FITPASSConfig.FITFEAST_4DAY_BEFORE_REMINDER_BODY)
                      message = sendgrid.Mail()
                      message.add_to(FITPASSConfig.EMAIL_TO.islower())
                      message.set_from(FITPASSConfig.EMAIL_FROM)
                      message.set_subject(userDetails['team_zone_name'] + "| FITFEAST Membership Expiry")
                      message.set_html(mailBody)
                      message.add_bcc(FITPASSConfig.EMAIL_TO)
                      message.add_cc(FITPASSConfig.EMAIL_TO)
                      message.set_replyto(FITPASSConfig.EMAIL_REPLY_TO)
                      response = SendGrid.send(message)
                      exit()

    def fitfeastmembershipexpiresin24hours(self):
        today = DT.date.today()
        next7daysDate = today + DT.timedelta(days=+1)
        MySqlConnection.cursor.execute("select team_zone_name,user_id,user_membership_id,user_id,user_email,user_name,diet_end_of_cycle from fitpass_users where  diet_end_of_cycle  between %s and %s ", (today, next7daysDate))
        userarray = MySqlConnection.cursor.fetchall()
        if userarray is not None:
            for userDetails in userarray:
                if FITPASSConfig.getCustomerSupportEmailAddress(userDetails['team_zone_name'])['customer_support'] != FITPASSConfig.FITPASS_REPLY_CUSTOMER_SUPPORT:
                    reply_email_address = FITPASSConfig.getCustomerSupportEmailAddress(userDetails['team_zone_name'])[
                                              'customer_support'] + ',' + FITPASSConfig.FITPASS_REPLY_CUSTOMER_SUPPORT
                else:
                    reply_email_address = FITPASSConfig.FITPASS_REPLY_CUSTOMER_SUPPORT

                with open('templete/mail/email-to-customer-fitfeast-membership-renewal-reminder-after-30days.html', 'r') as f:
                    html = f.read()
                    mailBody = html.replace("#USER_NAME#", userDetails['user_name']).replace("#MAIL_BODY#",FITPASSConfig.FITFEAST_1DAY_BEFORE_REMINDER_BODY)
                    message = sendgrid.Mail()
                    message.add_to(FITPASSConfig.EMAIL_TO.islower())
                    message.set_from(FITPASSConfig.EMAIL_FROM)
                    message.set_subject(userDetails['team_zone_name'] + "|  FITFEAST Membership Expiry")
                    message.set_html(mailBody)
                    message.add_bcc(FITPASSConfig.EMAIL_TO)
                    message.add_cc(FITPASSConfig.EMAIL_TO)
                    message.set_replyto(FITPASSConfig.EMAIL_REPLY_TO)
                    response = SendGrid.send(message)
                    exit()

    def fitfeastmembershipexpiredafter7days(self):
        today = DT.date.today()
        next7daysDate = today + DT.timedelta(days=-7)
        MySqlConnection.cursor.execute("select team_zone_name,user_id,user_membership_id,user_id,user_email,user_name,diet_end_of_cycle from fitpass_users where  diet_end_of_cycle  between %s and %s ", (next7daysDate, today))
        userarray = MySqlConnection.cursor.fetchall()
        if userarray is not None:
            for userDetails in userarray:
                if FITPASSConfig.getCustomerSupportEmailAddress(userDetails['team_zone_name'])['customer_support'] != FITPASSConfig.FITPASS_REPLY_CUSTOMER_SUPPORT:
                    reply_email_address = FITPASSConfig.getCustomerSupportEmailAddress(userDetails['team_zone_name'])[
                                              'customer_support'] + ',' + FITPASSConfig.FITPASS_REPLY_CUSTOMER_SUPPORT
                else:
                    reply_email_address = FITPASSConfig.FITPASS_REPLY_CUSTOMER_SUPPORT

                with open('/var/www/html/crons_project/templete/mail/email-to-customer-fitfeast-membership-renewal-reminder-after-30days.html', 'r') as f:
                    html = f.read()
                    mailBody = html.replace("#USER_NAME#", userDetails['user_name']).replace("#MAIL_BODY#",FITPASSConfig.FITFEAST_7DAY_AFTER_REMINDER_BODY)
                    message = sendgrid.Mail()
                    message.add_to(FITPASSConfig.EMAIL_TO.islower())
                    message.set_from(FITPASSConfig.EMAIL_FROM)
                    message.set_subject(userDetails['team_zone_name'] + "|  FITFEAST Membership Expiry")
                    message.set_html(mailBody)
                    message.add_bcc(FITPASSConfig.EMAIL_TO)
                    message.add_cc(FITPASSConfig.EMAIL_TO)
                    message.set_replyto(FITPASSConfig.EMAIL_REPLY_TO)
                    response = SendGrid.send(message)
                    exit()

    def fitfeastmembershipexpiredafter30days(self):
        today = DT.date.today()
        next7daysDate = today + DT.timedelta(days=-30)
        MySqlConnection.cursor.execute("select team_zone_name,user_id,user_membership_id,user_id,user_email,user_name,diet_end_of_cycle from fitpass_users where  diet_end_of_cycle  between %s and %s ", (next7daysDate, today))
        userarray = MySqlConnection.cursor.fetchall()
        if userarray is not None:
            for userDetails in userarray:
                if FITPASSConfig.getCustomerSupportEmailAddress(userDetails['team_zone_name'])['customer_support'] != FITPASSConfig.FITPASS_REPLY_CUSTOMER_SUPPORT:
                    reply_email_address = FITPASSConfig.getCustomerSupportEmailAddress(userDetails['team_zone_name'])[
                                              'customer_support'] + ',' + FITPASSConfig.FITPASS_REPLY_CUSTOMER_SUPPORT
                else:
                    reply_email_address = FITPASSConfig.FITPASS_REPLY_CUSTOMER_SUPPORT

                with open('templete/mail/email-to-customer-fitfeast-membership-renewal-reminder-after-30days.html', 'r') as f:
                    html = f.read()
                    mailBody = html.replace("#USER_NAME#", userDetails['user_name']).replace("#MAIL_BODY#",FITPASSConfig.FITFEAST_30DAY_AFTER_REMINDER_BODY)
                    message = sendgrid.Mail()
                    message.add_to(FITPASSConfig.EMAIL_TO.islower())
                    message.set_from(FITPASSConfig.EMAIL_FROM)
                    message.set_subject(userDetails['team_zone_name'] + "|  FITFEAST Membership Expiry")
                    message.set_html(mailBody)
                    message.add_bcc(FITPASSConfig.EMAIL_TO)
                    message.add_cc(FITPASSConfig.EMAIL_TO)
                    message.set_replyto(FITPASSConfig.EMAIL_REPLY_TO)
                    response = SendGrid.send(message)
                    exit()

    def coachmembershipreminder(self):
        today = DT.date.today()
        next7daysDate = today + DT.timedelta(days=+7)
        MySqlConnection.cursor.execute("select team_zone_name,user_id,user_membership_id,user_id,user_email,user_name,fitcoach_end_of_cycle from fitpass_users where  fitcoach_end_of_cycle  between %s and %s ", (today, next7daysDate))
        userarray = MySqlConnection.cursor.fetchall()
        if userarray is not None:
            for userDetails in userarray:
                if FITPASSConfig.getCustomerSupportEmailAddress(userDetails['team_zone_name'])['customer_support'] != FITPASSConfig.FITPASS_REPLY_CUSTOMER_SUPPORT:
                    reply_email_address = FITPASSConfig.getCustomerSupportEmailAddress(userDetails['team_zone_name'])[
                                              'customer_support'] + ',' + FITPASSConfig.FITPASS_REPLY_CUSTOMER_SUPPORT
                else:
                    reply_email_address = FITPASSConfig.FITPASS_REPLY_CUSTOMER_SUPPORT

                with open('/var/www/html/crons_project/templete/mail/email-to-customer-fitcoach-membership-renewal-reminder.html', 'r') as f:
                    html = f.read()
                    mailBody = html.replace("#USER_NAME#", userDetails['user_name'])
                    message = sendgrid.Mail()
                    message.add_to(FITPASSConfig.EMAIL_TO.islower())
                    message.set_from(FITPASSConfig.EMAIL_FROM)
                    message.set_subject(userDetails['team_zone_name'] + "|  FITCOACH Membership Expiry")
                    message.set_html(mailBody)
                    message.add_bcc(FITPASSConfig.EMAIL_TO)
                    message.add_cc(FITPASSConfig.EMAIL_TO)
                    message.set_replyto(FITPASSConfig.EMAIL_REPLY_TO)
                    response = SendGrid.send(message)
                    exit()


if __name__ == '__main__':
    if sys.argv[1] == 'fitpassmembershipexpiresinsevendays':
        Membershiprenewreminder.fitpassmembershipexpiresinsevendays(str)

    elif sys.argv[1] == 'fitpassmembershipexpiresin24hours':
        Membershiprenewreminder.fitpassmembershipexpiresin24hours(str)

    elif sys.argv[1] == 'fitpassmembershipexpiresintoday':
        Membershiprenewreminder.fitpassmembershipexpiresintoday(str)

    elif sys.argv[1] == 'fitpassmembershipexpiredafter7days':
        Membershiprenewreminder.fitpassmembershipexpiredafter7days(str)

    elif sys.argv[1] == 'fitpassmembershipexpiredafter30days':
        Membershiprenewreminder.fitpassmembershipexpiredafter30days(str)

    elif sys.argv[1] == 'fitfeastmembershipexpiresinfourdays':
        Membershiprenewreminder.fitfeastmembershipexpiresinfourdays(str)

    elif sys.argv[1] == 'fitfeastmembershipexpiresin24hours':
        Membershiprenewreminder.fitfeastmembershipexpiresin24hours(str)

    elif sys.argv[1] == 'fitfeastmembershipexpiredafter7days':
        Membershiprenewreminder.fitfeastmembershipexpiredafter7days(str)

    elif sys.argv[1] == 'fitfeastmembershipexpiredafter30days':
        Membershiprenewreminder.fitfeastmembershipexpiredafter30days(str)

    elif sys.argv[1] == 'coachmembershipreminder':
        Membershiprenewreminder.coachmembershipreminder(str)




