
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,700i" rel="stylesheet">
    </head>
    <body>
        <div style="background-color:#f5f5f5; padding-top:10px; padding-bottom:10px;">
            <table cellspacing="0" align="center" cellpadding="0" style=" margin:o auto; background-color:#fff;color:#29303f;width:100%;max-width:400px;font-family: 'Roboto', sans-serif;margin:0 auto;font-size:12px">
                <tbody>
                    <tr>
                        <td align="center" style="padding:20px; padding-bottom:20px;"><a href="https://fitpass.co.in/" target="new"><img src="https://fitpass-images.s3.amazonaws.com/gallery_image_fitpass_logo_vr_FF53.png" style="width:156px; height:auto;" /></a></td>
                    </tr>
                    <tr>
                        <td style="padding:5px 0 5px 0; background:#fff; height:180px; width:600px;"><img src="https://fitpass-images.s3.amazonaws.com/gallery_image_hyd_92A2.gif" style="width:400px; height:auto" /></td>
                    </tr>
                    <tr>
                        <td style="padding:10px 10px 0 10px; font-family:Helvetica,Arial,sans-serif; font-weight:700; font-size:22px;color:#222222; letter-spacing:0.5px; line-height:28px; text-align: center;">Workout anytime, anywhere.</td>
                    </tr>
                    <tr>
                        <td style="padding:10px 10px 0 10px; font-family:Helvetica,Arial,sans-serif; font-weight:700; font-size:12px; color:#222222; line-height:20px; text-align: center;">Get access to 150+ gyms/fitness centres across your city, so you can get into the perfect routine - wherever you might be.

                        </td>
                    </tr>
                    <tr>
                        <td style="padding:10px 10px 0 10px; font-family:Helvetica,Arial,sans-serif; font-weight:700; font-size:14px;color:#222222; letter-spacing:0.5px; line-height:28px; text-align: center;">Buy 3 months & get 1 month free</td>
                    </tr>
                    <tr>
                        <td style="padding:10px 10px 0 10px; font-family:Helvetica,Arial,sans-serif; font-weight:700; font-size:14px;color:#222222; letter-spacing:0.5px; line-height:28px; text-align: center;">on your fit-journey</td>
                    </tr>
                    <!--- Start btn --->
                    <tr>
                        <td style="padding:15px 10px 15px 10px; line-height:1" align="center">
                            <table border="0" cellspacing="0" cellpadding="0" style="width:160px;background-color:#ffffff;border:3px solid #000000">
                                <tbody><tr>
                                        <td align="center" height="35" style="height:35px">
                                            <a href="https://fitpass.co.in/register" style="display:inline-block;padding:10px 10px;font-size:12px; font-family:Helvetica,Arial,sans-serif;color:#000000;font-weight:700;text-decoration:none;letter-spacing:.5px" target="_blank" >GET FIT NOW</a></td></tr>
                                </tbody></table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" bgcolor="#f6f7f7" >
                            <!--- End Start btn --->
                            <table width="380" align="center" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td><tr>
                                            <td style="padding:20px 10px 20px 10px; font-family: 'Roboto', sans-serif; text-align:left; font-weight:400; line-height:16px; font-size:10px; color:#777777;"><strong>New members only.</strong>To be eligible, you must have received this email directly from FITPASS. For the intended recipient of this email, <a href="mailto:<?php echo $arrayData['emailTo']; ?>"><?php echo $arrayData['emailTo']; ?></a> only and is not transferable. Cannot be combined with any other offer. To redeem, you must follow promotional link in this email. Limited time only. We may revoke or modify offer anytime. FITPASS reserves the right to cancel any ineligible offer redemptions. Trial begins at the moment of sign up. The FITPASS membership gives you access to fitness centres to reserve workouts. For additional terms, see our <a href="https://fitpass.co.in/terms-and-conditions" style=" text-decoration:underline; color:#000;">Terms & Conditions</a> and <a href="https://fitpass.co.in/privacy-policy" style="text-decoration:underline; color:#000;">Privacy Policy.</a> Other restrictions may apply.</td>
                                        </tr>
                                        <tr>
                                            <td align="center">
                                                <table cellspacing="0" align="center" cellpadding="0" style="width:220px; height:25px;  padding-right:10px;">
                                                    <tbody>   
                                                        <tr>
                                                            <td align="center" style="padding:5px;"><a href="https://www.facebook.com/fitpassindia" style="text-decoration:none;"><img src="https://fitpass-images.s3.amazonaws.com/gallery_image_fb_ic_4862.png" width="25" height="25"></a></td>
                                                            <td align="center" style="padding:5px;"><a href="https://twitter.com/fitpassindia/" style="text-decoration:none;"><img src="https://fitpass-images.s3.amazonaws.com/gallery_image_tw_ic_8DE1.png" width="25" height="25"></a></td>
                                                            <td align="center" style="padding:5px;"><a href="https://www.instagram.com/fitpassindia/" style="text-decoration:none;"><img src="https://fitpass-images.s3.amazonaws.com/gallery_image_int_ic_88D1.png" width="25" height="25"></a></td>
                                                            <td align="center" style="padding:5px;"><a href="https://fitpass.co.in/" style="text-decoration:none;"><img src="https://fitpass-images.s3.amazonaws.com/gallery_image_web_ic_58DD.png" width="25" height="25"></a></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td></tr>
                                        <tr>
                                            <td align="center">
                                                <table cellpadding="0" cellspacing="0" style="width:100%; font-size:12px">
                                                    <tbody>
                                                        <tr><td colspan="2" style="font-family: 'Roboto', sans-serif; font-weight:400; color:#10314f; padding:5px 0 5px 0; font-size:14px; text-align:center;">Get fit with just a tap! <span style="font-family: 'Roboto', sans-serif; font-weight:700; font-size:14px; color:#10314f;">GET THE APP</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right" style="font-weight:400; color:#2c485b; padding-right:10px;"><a href="https://fitpass.co.in/app" style="text-decoration:none;"><img src="https://fitpass-images.s3.amazonaws.com/gallery_image_app_store_ic_DE30.png" width="99" height="30" /></a></td>
                                                            <td align="left"style="font-weight:500;color:#0e314e; padding-left:10px;"><a href="https://fitpass.co.in/app" style="text-decoration:none;"> <img src="https://fitpass-images.s3.amazonaws.com/gallery_image_plat_store_25BE.png" width="99" height="30" /></a></td>
                                                        </tr>
                                                        <tr><td colspan="2" style="font-family: 'Roboto', sans-serif; font-weight:400; color:#10314f; padding:10px 0 5px 0; font-size:12px; text-align:center;"> <a href="mailto:care@fitpass.co.in" style="font-family: 'Roboto', sans-serif; font-weight:500; font-size:12px; text-decoration:none; color:#10314f;">care@fitpass.co.in</a></td>
                                                        </tr>
                                                        <tr><td colspan="2"><hr style="color:#CCC; size:0.5px;" /> </td>
                                                        </tr>
                                                        <tr><td colspan="2" style="font-family: 'Roboto', sans-serif; font-weight:400; color:#10314f; padding:5px 0 5px 0; letter-spacing:1px; font-size:10px; text-align:center;"> ©2018 FITPASS.co.in | All Rights Reserved. </td>
                                                        </tr>

                                                    </tbody>
                                                </table></td>
                                        </tr>
                                        </table>

                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            </body>
                            </html>
<?php #echo die;?>