
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,700i" rel="stylesheet">
</head>

<body>
<div style="background-color:#f5f5f5; padding-top:10px; padding-bottom:10px;">
<table cellspacing="0" align="center" cellpadding="0" style=" margin:o auto; background-color:#fff;color:#29303f;width:100%;max-width:400px;font-family: 'Roboto', sans-serif;margin:0 auto;font-size:12px">
  <tbody>
     <tr>
        <td style="width:100%; height:70px;background:url(https://fitpass-images.s3.amazonaws.com/gallery_image_header-bg_A76D.png) top center no-repeat; background-size:contain;">
        <table cellspacing="0" align="right" cellpadding="0" style="width:300px; height:70px;  padding-right:10px;">
            <tbody>   
              <tr>
                <td align="right"><a href="mailto:partners@fitpass.co.in" style="font-weight:400;text-decoration:none;color:#fff;"><img src="https://fitpass-images.s3.amazonaws.com/gallery_image_mail_ic_246C.png" style="margin-right:5px; width:14px; height:12px; vertical-align:middle;">partners@fitpass.co.in</a></td>
              </tr>
            </tbody>
          </table></td>
      </tr>
    <tr>
      <td style="padding:10px 10px 0 10px; font-family: 'Roboto', sans-serif; font-weight:500; font-size:12px;color:#222222;text-align:left;">Dear Partner,</td>
    </tr>
    <tr>
      <td style="padding:10px 10px 0 10px; font-family: 'Roboto', sans-serif; font-weight:400; font-size:12px;color:#222222; letter-spacing:1px; line-height:18px;text-align:left;">Please find below user details for the latest workout reserved.</td>
    </tr>
    <tr>
      <td style="padding:20px 10px 0 10px; font-family: 'Roboto', sans-serif; font-weight:500; font-size:12px;color:#a2a1a1; text-align:left;">User Details</td>
    </tr>
    <tr>
      <td style="padding:10px 0 10px 0;line-height:1"><table cellpadding="0" cellspacing="0" style="width:400px;font-size:12px;background-color:#F8FAFD">
          <tbody>
            <tr>
              <td style="padding:10px;line-height:1.4" align="center"><table cellpadding="10" align="center" cellspacing="0" style="width:380px;font-size:12px;">
                  <tbody>
                    
                    <tr>
                      <td width="30%" style="font-weight:500;color:#222222;font-size:12px; border-bottom-style:inset;border-bottom-width:thin;text-align:left"> FITPASS ID </td>
                      <td width="70%" style="font-weight:500;color:#222222;font-size:12px; border-bottom-style:inset;border-bottom-width:thin;text-align:right"> <?php echo $arrayData['membership_id']; ?> </td>
                    </tr>
                    <tr>
                      <td width="30%" style="font-weight:500;color:#222222; font-size:12px; border-bottom-style:inset;border-bottom-width:thin;text-align:left"> Name </td>
                      <td width="70%" style="font-weight:500;color:#222222; font-size:12px; border-bottom-style:inset;border-bottom-width:thin;text-align:right"> <?php echo ucwords($arrayData['user_name']); ?> </td>
                    </tr>
                    <tr>
                      <td width="30%" style="font-weight:500;color:#222222;border-bottom-style:inset;border-bottom-width:thin;text-align:left"> Workout </td>
                      <td width="70%" style="font-weight:500;color:#222222;border-bottom-style:inset;border-bottom-width:thin;text-align:right"> <?php echo $arrayData['workout_name']; ?> </td>
                    </tr>
                    <tr>
                      <td width="30%" style="font-weight:500;color:#222222;border-bottom-style:inset;border-bottom-width:thin;text-align:left"> Date & Time </td>
                      <td width="70%" style="font-weight:500;color:#222222;border-bottom-style:inset;border-bottom-width:thin;text-align:right"> <?php echo $arrayData['date_of_workout']; ?> | <?php echo $arrayData['workout_time']; ?> </td>
                    </tr>
                  </tbody>
                </table></td>
            </tr>
          </tbody>
        </table></td>
    </tr>
      <tr>
      <td style="padding:15px 10px 15px 10px;line-height:1" align="center">
                            <a href="http://studios.fitpass.co.in" style="width:220px; background: -moz-linear-gradient(45deg, rgba(49,9,100,1) 0%, rgba(222,77,90,1) 100%); /* ff3.6+ */
                               background: -webkit-gradient(linear, left bottom, right top, color-stop(0%, rgba(49,9,100,1)), color-stop(100%, rgba(222,77,90,1))); /* safari4+,chrome */
                               background: -webkit-linear-gradient(45deg, rgba(49,9,100,1) 0%, rgba(222,77,90,1) 100%); /* safari5.1+,chrome10+ */
                               background: -o-linear-gradient(45deg, rgba(49,9,100,1) 0%, rgba(222,77,90,1) 100%); /* opera 11.10+ */
                               background: -ms-linear-gradient(45deg, rgba(49,9,100,1) 0%, rgba(222,77,90,1) 100%); /* ie10+ */
                               background: linear-gradient(45deg, rgba(49,9,100,1) 0%, rgba(222,77,90,1) 100%); /* w3c */
                               filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#DE4D5A', endColorstr='#310964',GradientType=1 ); /* ie6-9 */ display:block; font-family: 'Roboto', sans-serif; text-transform:uppercase; letter-spacing:1px; color:#fff; padding:15px 0; font-size:12px; text-align:center;border-radius:3px;text-decoration:none;font-weight:bold" target="_blank"> Manage User </a></td>
                               </tr>
   
    <tr>
      <td style="padding:10px 0 20px 10px; font-family: 'Roboto', sans-serif; font-weight:500; font-size:12px; color:#222222; text-align:left;">Cheers!<br/>Team <span style="font-family: 'Roboto', sans-serif; font-weight:700; font-size:12px; color:#E04e4e;">FITPASS</span></td>
    </tr>
    <tr>
      <td style="background-color:#FBFBFB;"><table cellpadding="0" cellspacing="0" style="font-size:12px;border-radius:0 3px 3px 0; width:400px;">
          <tbody>
            <tr>
              <td width="65" style="padding:20px 0 0 10px;vertical-align:top"><img src="https://fitpass-images.s3.amazonaws.com/gallery_image_contact_ic_4EB5.png" width="40" height="40" style="width:40px"></td>
              <td width="333" style="padding:20px 0 20px 0"><h3 style="margin:0;font-size:14px; color:#222222;">Get in touch with us!</h3>
                <p style="margin:5px 0"><a href="mailto:partners@fitpass.co.in" style="font-weight:400;text-decoration:none;color:#222222">partners@fitpass.co.in</a> &nbsp; | &nbsp; <a href="tel:011-46061468" style="font-weight:400;text-decoration:none;color:#222222">011-46061468</a></p></td>
            </tr>
          </tbody>
        </table></td>
    </tr>
    <tr>
      <td style="background-color:#ffffff; padding:10px 10px 20px 10px;" align="center"><table cellpadding="0" cellspacing="0" style="width:100%;font-size:12px">
          <tbody>
            <tr>
              <td colspan="2" style="font-weight:500; color:#10314f; padding:5px 0; font-size:14px; text-align:center; text-transform:uppercase;">Download the FITPASS partner app!</td>
            </tr>
            <tr>
              <td align="right" style="font-weight:400; color:#2c485b; padding-right:10px;"><a href="https://fitpass.co.in/partner-app" style="text-decoration:none;"><img src="https://fitpass-images.s3.amazonaws.com/gallery_image_app_store_ic_9AA3.png" width="99" height="30" /></a></td>
              <td align="left"style="font-weight:500;color:#0e314e; padding-left:10px;"><a href="https://fitpass.co.in/partner-app" style="text-decoration:none;"> <img src="https://fitpass-images.s3.amazonaws.com/gallery_image_play_store_AFCC.png" width="99" height="30" /></a></td>
            </tr>
          </tbody>
        </table></td>
    </tr>
    <tr>
      <td style="padding:0px 20px 20px 20px; font-family: 'Roboto', sans-serif; text-align:center; font-weight:400; font-size:10px; color:#777777;">You are receiving this email because you are one of our partners and have agreed for this mutual collaboration.</td>
    </tr>
  </tbody>
</table>
</body>
</html>
