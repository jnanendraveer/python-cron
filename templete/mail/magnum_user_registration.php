
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,700i" rel="stylesheet">
</head>

<body>
<div style="background-color:#f5f5f5; padding-top:10px; padding-bottom:10px;">
<table cellspacing="0" align="center" cellpadding="0" style=" margin:o auto; background-color:#fff;color:#29303f;width:100%;max-width:400px;font-family: 'Roboto', sans-serif;margin:0 auto;font-size:12px">
  <tbody>
     <tr>
        <td style="width:100%; height:70px;background:url(https://fitpass-images.s3.amazonaws.com/gallery_image_header_ic_853C.jpg) top center no-repeat; background-size:contain;">
       </td>
      </tr>
    <tr>
      <td style="padding:5px 10px 0 10px; font-family: 'Roboto', sans-serif; font-weight:500; font-size:12px;color:#222222;text-align:left;">Hi <?php echo $arrayData['user_name'];?>,</td>
    </tr>
    <tr>
      <td style="padding:10px 10px 0 10px; font-family: 'Roboto', sans-serif; font-weight:400; font-size:12px;color:#222222; letter-spacing:1px; line-height:18px;text-align:left;">Thank you for showing an interest in FITPASS Magnum.</td>
    </tr>
    <tr>
      <td style="padding:10px 10px 15px 10px; font-family: 'Roboto', sans-serif; font-weight:500; font-size:12px;color:#222222; letter-spacing:0.5px; line-height:18px;text-align:left;">FITPASS Magnum is an ultimate 360-degree advantage for you & your family’s well being.</td>
    </tr>
    <tr>
      <td valign="top"><img src="https://fitpass-images.s3.amazonaws.com/gallery_image_header_ic_99BF.jpg" style="width:400px; height:auto; vertical-align:top;" /></td>
    </tr>
    <tr>
      <td valign="top"><img src="https://fitpass-images.s3.amazonaws.com/gallery_image_header_1_ic_0442.jpg" style="width:400px; height:auto; vertical-align:top;" /></td>
    </tr>
    <tr>
      <td valign="top"><img src="https://fitpass-images.s3.amazonaws.com/gallery_image_header_2_ic_65A4.jpg" style="width:400px; height:auto; vertical-align:top;" /></td>
    </tr>
    <tr>
      <td valign="top"><img src="https://fitpass-images.s3.amazonaws.com/gallery_image_header_3_ic_DECD.jpg" style="width:400px; height:auto; vertical-align:top;" /></td>
    </tr>
    <tr>
      <td valign="top"><img src="https://fitpass-images.s3.amazonaws.com/gallery_image_header_4_ic_78C0.jpg" style="width:400px; height:auto; vertical-align:top;" /></td>
    </tr>
    <tr>
      <td align="center" style="background-color:#0d1f35;">
      <table width="95%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="50%" style="padding:20px 0 0 0"><h3 style="margin:0;font-size:14px; font-weight:500; color:#ffffff;">Get in touch with us!</h3>
                <p style="margin:2px 0; color:#ffffff; line-height:16px;"><a href="mailto:magnumsupport@fitpass.co.in" style="font-weight:400;text-decoration:none;color:#ffffff">magnumsupport@fitpass.co.in</a><br/><a href="tel:011-23600777" style="font-weight:400;text-decoration:none;color:#ffffff">011-23600777</a></p></td>
    <td width="50%" valign="middle" align="right"><img src="https://fitpass-images.s3.amazonaws.com/gallery_image_fitpass_logo_12_399F.png" style="width:76px; height:auto; vertical-align:central" /></td>
  </tr>
  <tr>
    <td colspan="2"><table cellspacing="0" align="center" cellpadding="0" style="width:120px; height:40px;">
            <tbody>   
              <tr>
              <td align="left" style="padding:2px;"><a href="http://facebook.com/fitpassindia/" style="text-decoration:none;"><img src="https://fitpass-images.s3.amazonaws.com/gallery_image_fb_1EE9.png" width="20" height="20"></a></td>
              <td align="left" style="padding:2px;"><a href="https://twitter.com/fitpassindia/" style="text-decoration:none;"><img src="https://fitpass-images.s3.amazonaws.com/gallery_image_tw_CE5D.png" width="20" height="20"></a></td>
                <td align="left" style="padding:2px;"><a href="https://www.instagram.com/fitpassindia/" style="text-decoration:none;"><img src="https://fitpass-images.s3.amazonaws.com/gallery_image_in_3B2B.png" width="20" height="20"></a></td>
                <td align="left" style="padding:2px;"><a href="https://in.linkedin.com/company/fitpass" style="text-decoration:none;"><img src="https://fitpass-images.s3.amazonaws.com/gallery_image_li_27E5.png" width="20" height="20"></a></td>
              </tr>
            </tbody>
          </table></td>
    </tr>
      </table>
      </td>
    </tr>
    </tbody>   
</table>
</body>
</html>
