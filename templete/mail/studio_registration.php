
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,700i" rel="stylesheet">
</head>

<body>
<div style="background-color:#f5f5f5; padding-top:10px; padding-bottom:10px;">
    <table cellspacing="0" align="center" cellpadding="0" style="margin:0 auto; background-color:#fff;color:#29303f;width:100%;max-width:400px;font-family: 'Roboto', sans-serif;margin:0 auto;font-size:12px">
        <tbody>
        <tr>
            <td style="width:100%; height:70px;background:url(https://fitpass-images.s3.amazonaws.com/gallery_image_header-bg_A76D.png) top center no-repeat; background-size:contain;">
                <table cellspacing="0" align="right" cellpadding="0" style="width:300px; height:70px;  padding-right:10px;">
                    <tbody>
                    <tr>
                        <td align="right"><a href="mailto:partners@fitpass.co.in" style="font-weight:400;text-decoration:none;color:#fff;"><img src="https://fitpass-images.s3.amazonaws.com/gallery_image_mail_ic_246C.png" style="margin-right:5px; width:14px; height:12px; vertical-align:middle;">partners@fitpass.co.in</a></td>
                    </tr>
                    </tbody>
                </table></td>
        </tr>
        <tr>
            <td style="padding:10px 10px 0 10px; font-family: 'Roboto', sans-serif; font-weight:500; font-size:12px;color:#222222; text-align:left;">Dear <?php echo $arrayData['studio_name']; ?>,</td>
        </tr>
        <tr>
            <td style="padding:10px 10px 0 10px; font-family: 'Roboto', sans-serif; font-weight:500; font-size:12px; color:#444444; text-align:center;"><samp style="font:17px/25px Roboto, sans-serif;"><strong>Greetings from fitpass!</strong></samp></td>
        </tr>
        <tr>
            <td style="padding:10px 10px 0 10px; font-family: 'Roboto', sans-serif; font-weight:400; color:#444444; text-align:left;">We are delighted to partner with you and embark on a revolutionary fitness journey together. Based on our discussions, we would like to share your profile with you.</td>
        </tr>
        <tr>
            <td style="padding:10px 10px 0 10px; font-family: 'Roboto', sans-serif; font-weight:400; color:#444444; text-align:left;">Please find below the link to your profile and kindly follow the steps to smoothly complete the registration process.</td>
        </tr>
        <tr>
            <td style="padding:10px 10px 0 10px; font-family: 'Roboto', sans-serif; font-weight:400; color:#444444; text-align:center;"><samp style="font:17px/25px Roboto, sans-serif;"><strong>Instructions</strong></samp></td>
        </tr>
        <tr>
            <td align="center" style="padding-top: 10px; padding-bottom: 15px; margin:0 auto;">
                <table width="90%" align="center" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="15%" style="vertical-align:middle; padding:4px; font-family:'Roboto', sans-serif; font-weight:500; color:#000; text-align:left;">Step 1-</td>
                        <td width="85%" style="vertical-align:middle; padding:4px; font-family:'Roboto', sans-serif; font-weight:400; color:#000; text-align:left;"><a href="<?php echo 'http://studios.fitpass.co.in/studio/' . $arrayData['public_url']; ?>">Click here</a> and fill up correct and up-to-date information.</td>
                    </tr>
                    <tr>
                        <td width="15%" style="vertical-align:middle; padding:4px; font-family:'Roboto', sans-serif; font-weight:500; color:#000; text-align:left;">Step 2-</td>
                        <td width="85%" style="vertical-align:middle; padding:4px; font-family:'Roboto', sans-serif; font-weight:400; color:#000; text-align:left;">Read and agree to the Terms & Conditions.</td>
                    </tr>
                    <tr>
                        <td width="15%" style="vertical-align:text-top; padding:4px; font-family:'Roboto', sans-serif; font-weight:500; color:#000; text-align:left;">Step 3-</td>
                        <td width="85%" style="vertical-align:middle; padding:4px; font-family:'Roboto', sans-serif; font-weight:400; color:#000; text-align:left;">Upon completion of Step 2, you will receive an automated email with your Username and a One-Time-Password to access your profile. Please change the automated One-Time-Password, shared with you in a separate email, with a new password for your profile.
                        </td>
                    </tr>
                    <tr>
                        <td width="15%" style="vertical-align:text-top; padding:4px; font-family:'Roboto', sans-serif; font-weight:500; color:#000; text-align:left;">Step 4-</td>
                        <td width="85%" style="vertical-align:middle; padding:4px; font-family:'Roboto', sans-serif; font-weight:400; color:#000; text-align:left;">Make relevant edits to your classes, add new classes and batches as per your updated schedules, etc.</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="padding:20px 0 20px 10px; font-family: 'Roboto', sans-serif; font-weight:500; font-size:12px; color:#222222; text-align:left;">Cheers!<br/>Team <span style="font-family: 'Roboto', sans-serif; font-weight:700; font-size:12px; color:#E04e4e;">FITPASS</span></td>
        </tr>
        <tr>
            <td style="background-color:#FBFBFB;"><table cellpadding="0" cellspacing="0" style="font-size:12px;border-radius:0 3px 3px 0; width:400px;">
                    <tbody>
                    <tr>
                        <td width="65" style="padding:20px 0 0 10px;vertical-align:top"><img src="https://fitpass-images.s3.amazonaws.com/gallery_image_contact_ic_4EB5.png" width="40" height="40" style="width:40px"></td>
                        <td width="333" style="padding:20px 0 20px 0"><h3 style="margin:0;font-size:14px; color:#222222;">Get in touch with us!</h3>
                            <p style="margin:5px 0"><a href="mailto:partners@fitpass.co.in" style="font-weight:400;text-decoration:none;color:#222222">partners@fitpass.co.in</a> &nbsp; | &nbsp; <a href="tel:011-46061468" style="font-weight:400;text-decoration:none;color:#222222">011-46061468</a></p></td>
                    </tr>
                    </tbody>
                </table></td>
        </tr>
        <tr>
            <td style="background-color:#ffffff; padding:10px 10px 20px 10px;" align="center"><table cellpadding="0" cellspacing="0" style="width:100%;font-size:12px">
                    <tbody>
                    <tr>
                        <td colspan="2" style="font-weight:500; color:#10314f; padding:5px 0; font-size:14px; text-align:center; text-transform:uppercase;">Download the FITPASS partner app!</td>
                    </tr>
                    <tr>
                        <td align="right" style="font-weight:400; color:#2c485b; padding-right:10px;"><a href="https://fitpass.co.in/partner-app" style="text-decoration:none;"><img src="https://fitpass-images.s3.amazonaws.com/gallery_image_app_store_ic_9AA3.png" width="99" height="30" /></a></td>
                        <td align="left"style="font-weight:500;color:#0e314e; padding-left:10px;"><a href="https://fitpass.co.in/partner-app" style="text-decoration:none;"> <img src="https://fitpass-images.s3.amazonaws.com/gallery_image_play_store_AFCC.png" width="99" height="30" /></a></td>
                    </tr>
                    </tbody>
                </table></td>
        </tr>
        <tr>
            <td style="padding:0px 20px 20px 20px; font-family: 'Roboto', sans-serif; text-align:center; font-weight:400; font-size:10px; color:#777777;">You are receiving this email because you are one of our partners and have agreed for this mutual collaboration.</td>
        </tr>
        </tbody>
    </table>
</div>
</body></html>