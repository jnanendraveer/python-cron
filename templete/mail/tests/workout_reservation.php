<?php include 'mailheader.php' ?>
<tr>
    <td align="center" style="background-color:#FFF"><div>
            <table width="90%" border="0" cellspacing="0" cellpadding="0" style="padding-top:20px;">
                <tbody>
                    <tr>
                        <td style="font-size:16px;font-family: 'Roboto', sans-serif;color:rgb(51,51,51);text-align:left;line-height:30px; padding-bottom:5px;">Dear <?php echo ucwords(strtolower($arrayData['user_name'])); ?>!, </td>
                    </tr>
                    <tr>
                        <td style="font-size:20px;font-family: 'Roboto', sans-serif;color:#00a550; font-weight:700; font-style:italic; text-align:left;line-height:30px;">Congratulations!</td>
                    </tr>
                    <tr>
                        <td style="font-size:20px;font-family: 'Roboto', sans-serif;color:#000000; font-weight:700; text-align:left;line-height:30px; padding-bottom:10px;">Your workout has been reserved.</td>
                    </tr>
                    <tr>
                        <td style="font-size:16px;font-family: 'Roboto', sans-serif;color:rgb(51,51,51);text-align:left;line-height:24px; padding-bottom:20px;">
                            Kindly carry a photo ID card along with you when you go for this workout. Upon reaching the Fitness Centre, please scan the QR code available at the reception. </td>
                    </tr>
                    <tr>
                        <td style="font-size:16px;font-family: 'Roboto', sans-serif;color:rgb(51,51,51);text-align:left;line-height:24px; padding-bottom:20px;">
                            If you wish to, you may cancel your workout from your FITPASS History on your profile, on the app. Do remember, you can cancel the workout up to 1.5 hours before your workout.
                    </tr>
                </tbody>
            </table>
        </div></td>
</tr>
<tr>
    <td align="center" style="background-color:white;padding-bottom:20px">
        <table width="90%" border="0" bgcolor="#8d8f9c" style="font-size:14px; font-family: 'Roboto', sans-serif; color:#FFF; font-weight:300; padding:5px 0 5px 0;">
            <tr>
                <th scope="col" style="font-size:16px; font-family: 'Roboto', sans-serif; font-weight:400; text-align:left; padding-left:10px;">Studio/Gyms Details</th>
            </tr>
        </table>
        <table width="90%" border="0" cellpadding="10" cellspacing="0" style="border:#999 solid 1px; ">
            <tr>
                <th width="50%" style="font-size:14px; font-family: 'Roboto', sans-serif; font-weight:400; line-height:24px; text-align:left; color:#424242;" scope="col">FITPASS ID<br/>
                    <span style="font-size:16px; font-family: 'Roboto', sans-serif; font-weight:400; text-align:left; color:#000000;"><?php echo $arrayData['membership_id']; ?></span></th>
                <th width="50%" style="font-size:14px; font-family: 'Roboto', sans-serif; font-weight:400; line-height:24px; text-align:left; color:#424242;" scope="col">Unique Reservation Code<br/>
                    <span style="font-size:16px; font-family: 'Roboto', sans-serif; font-weight:400; text-align:left; color:#000000;"><?php echo $arrayData['urc']; ?></span></th>
            </tr>
            <tr bgcolor="#eaeaea">
                <th scope="col" style="font-size:14px; font-family: 'Roboto', sans-serif; font-weight:400; line-height:24px; text-align:left; color:#424242;">Workout Name<br/>
                    <span style="font-size:16px; font-family: 'Roboto', sans-serif; font-weight:400; text-align:left; color:#000000;"><?php echo $arrayData['workout_name']; ?></span></th>
                <th scope="col" style="font-size:14px; font-family: 'Roboto', sans-serif; font-weight:400; line-height:24px; text-align:left; color:#424242;">DATE & TIME<br/>
                    <span style="font-size:16px; font-family: 'Roboto', sans-serif; font-weight:400; text-align:left; color:#000000;"><?php echo $arrayData['date_of_workout']; ?> | <?php echo $arrayData['workout_time']; ?></span></th>
            </tr>
            <tr>
                <th colspan="2" style="font-size:14px; font-family: 'Roboto', sans-serif; font-weight:400; line-height:24px; text-align:left; color:#424242;" scope="col">Studio Name & Address<br/>
                    <span style="font-size:16px; font-family: 'Roboto', sans-serif; font-weight:400; text-align:left; color:#000000;"><b><?php echo $arrayData['studio_name']; ?></b>- <?php echo $arrayData['Address']; ?></span></th>
            </tr>
        </table></td>
</tr>


<?php include 'mailfooter.php' ?>