<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />      
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,900" rel="stylesheet">
    </head>
    <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
        <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
            <tr>
                <td align="center" valign="top">
                    <!-- BEGIN TEMPLATE // -->
                    <table border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" width="600" style="width:600px;">
                        <tr>
                            <td align="center" valign="top">
                                <!-- BEGIN PREHEADER // -->
                                <table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ffffff">
                                    <tr>
                                        <td align="center" valign="top" style="padding-top:15px; padding-right:20px; padding-bottom:15px; padding-left:20px; margin:0 auto;" mc:edit="preheader_content"><a href="https://fitpass.co.in"><img src="img/logo.png"  style="text-align:center" /></a>
                                        </td>
                                    </tr>
                                </table>
                                <!-- // END PREHEADER -->
                            </td>
                        </tr>
                        <tr>
                            <td align="center" valign="top">
                                <!-- BEGIN HEADER // -->
                                <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateHeader">
                                    <tr>
                                        <td valign="top">
                                            <img src="<?php echo SITE_URL; ?>/emailer-images/dietitian/diet-banner-3.png" width="100%" />
                                        </td>
                                    </tr>
                                </table>
                                <!-- // END HEADER -->
                            </td>
                        </tr>
                        <tr>
                            <td align="center" valign="top">
                                <!-- BEGIN BODY // -->
                                <table border="0" cellpadding="0" cellspacing="0" width="90%" style="padding-bottom:20px;">
                                    <tr>
                                        <td valign="top" style="font-family:'Roboto', sans-serif; font-weight:400; font-size:16px; line-height:28px; padding-top:20px; color:#071e34;">
                                            Dear <?php echo $arrayData['user_name'];?>!
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" style="font-family:'Roboto', sans-serif; font-weight:400; font-size:16px; line-height:28px; padding-top:20px; color:#071e34;">
                                            Congratulations on making it this far!
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" style="font-family:'Roboto', sans-serif; font-weight:400; font-size:16px; line-height:28px; padding-top:20px; color:#071e34;">
                                            Your Diet Plan with FITPASS is about to expire on
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" style="font-family:'Roboto', sans-serif; font-weight:900; font-size:16px; line-height:28px; padding-top:0px; color:#071e34;">
                                           <?php echo $arrayData['diet_end_of_cycle'];?>.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" style="font-family:'Roboto', sans-serif; font-weight:400; font-size:16px; line-height:28px; padding-top:20px; color:#071e34;">
                                            Stay on top of your diet! 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" style="font-family:'Roboto', sans-serif; font-weight:400; font-size:16px; line-height:28px; padding-top:20px; color:#071e34;">
                                            Renew your Diet Plan and enjoy unlimited diet consultation on the FITPASS app.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" style="margin:0 auto;" align="center">
                                            <a href="#" style="text-decoration:none;" target="_blank"><p style="width:140px; background-color:#071e34; font-family:'Roboto', sans-serif; font-weight:900; font-size:16px; padding:10px; letter-spacing:2px; color:#ffffff; text-align:center;">RENEW NOW</p></a>
                                        </td>
                                    </tr>
                                </table>
                                <!-- // END BODY -->
                            </td>
                        </tr>
                        <tr><td valign="top" width="100%">
                                <table width="100%" border="0" cellspacing="0" bgcolor="#f2494d" style="vertical-align:top;">
                                    <tr>
                                        <td style="font-family:'Roboto', sans-serif; font-weight:400; font-size:22px; color:#ffffff; padding:5px; text-align:center; padding-top:20px;">GET THE FITPASS APP</td></tr>
                                    <tr>
                                        <td align="center" style="margin:0 auto; padding-bottom:20px; padding-top:10px;"><table width="300" border="0" cellspacing="0">
                                                <tr>
                                                    <td align="center"><a href="https://play.google.com/store/apps/details?id=com.india.fitpass&amp;referrer=tracking_id=web-fitpass-5256395210836" target="_blank" style="text-decoration:none;"><img src="<?php echo SITE_URL; ?>/emailer-images/playstore.png" width="128" height="42" /></a></td>
                                                    <td align="center"><a href="https://itunes.apple.com/nz/app/fitpass/id1049745078?mt=8" target="_blank" style="text-decoration:none;"><img src="<?php echo SITE_URL; ?>/emailer-images/appstore.png" width="128" height="42" /></a></td>
                                                </tr>
                                            </table>
                                        </td></tr>
                                    <tr><td align="center" valign="top" bgcolor="#071e34" style="padding-top:15px; padding-bottom:15px;">
                                            <table width="200" border="0" cellspacing="0">
                                                <tr>
                                                    <td align="center"><a href="https://www.facebook.com/fitpassindia" target="_blank"><img src="<?php echo SITE_URL; ?>/emailer-images/fb.png" width="40" height="40" /></a></td>
                                                    <td align="center"><a href="https://twitter.com/fitpassindia" target="_blank"><img src="<?php echo SITE_URL; ?>/emailer-images/tw.png" width="40" height="40" /></a></td>
                                                    <td align="center"><a href="https://www.instagram.com/fitpassindia/" target="_blank"><img src="<?php echo SITE_URL; ?>/emailer-images/im.png" width="40" height="40" /></a></td>
                                                </tr>
                                            </table>
                                        </td></tr>
                                    <tr><td align="center" bgcolor="#2d7dbc"> 
                                            <table width="300" border="0" cellspacing="0" bgcolor="#2d7dbc">
                                                <tr>
                                                    <td align="center" style="font-family:'Roboto', sans-serif; font-weight:500; font-size:14px; color:#ffffff;  padding:20px;"><a href="mailto:care@fitpass.co.in?Subject=Team%20Fitpass" target="_top" style="font-family:'Roboto', sans-serif; font-weight:500; font-size:14px; color:#ffffff; text-decoration:none;">care@fitpass.co.in</a>&nbsp;&nbsp;   |  &nbsp;&nbsp;<a href="tel:01146061468" style="font-family:'Roboto', sans-serif; font-weight:500; color:#ffffff; text-decoration:none;">011-46061468</a></td>
                                                </tr>
                                            </table>

                                        </td>
                                    </tr>
                                </table>
                                <!-- // END TEMPLATE -->
                            </td>
                        </tr>
                    </table>
                    </body>
                    </html>