<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,700i" rel="stylesheet">
</head>

<body>
<div style="background-color:#f5f5f5; padding-top:10px; padding-bottom:10px;">
    <table cellspacing="0" align="center" cellpadding="0" style="margin:0 auto; background-color:#fff;color:#29303f;width:100%;max-width:400px;font-family: 'Roboto', sans-serif;margin:0 auto;font-size:12px">
        <tbody>
        <tr>
            <td style="width:100%; height:70px;background:url(https://fitpass-images.s3.amazonaws.com/gallery_image_header-bg_A76D.png) top center no-repeat; background-size:contain;">
                <table cellspacing="0" align="right" cellpadding="0" style="width:300px; height:70px;  padding-right:10px;">
                    <tbody>
                    <tr>
                        <td align="right">
                            <a href="mailto:care@fitpass.co.in" style="font-weight:400;text-decoration:none;color:#fff;"><img src="https://fitpass-images.s3.amazonaws.com/gallery_image_mail_ic_246C.png" style="margin-right:5px; width:14px; height:12px; vertical-align:middle;">care@fitpass.co.in</a>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td style="padding:10px 10px 0 10px; font-family: 'Roboto', sans-serif; font-weight:500; font-size:12px;color:#222222; text-align:left;">
                Dear <?php echo ucwords($arrayData['user_name']); ?>,
            </td>
        </tr>
        <tr>
            <td style="padding:10px 10px 0 10px; font-family: 'Roboto', sans-serif; font-weight:400; color:#071e34; text-align:left;">
                Your issue has been resolved.
            </td>
        </tr>
        <tr>
            <td style="padding:10px 10px 0 10px; font-family: 'Roboto', sans-serif; font-weight:400; color:#071e34; text-align:left;">
                We’re always here to help you. We strive to provide a positive user experience to you in the future.
            </td>
        </tr>
        <tr>
            <td style="padding:20px 0 20px 10px; font-family: 'Roboto', sans-serif; font-weight:500; font-size:12px; color:#222222; text-align:left;">
                Cheers!<br/>Team
                <span style="font-family: 'Roboto', sans-serif; font-weight:700; font-size:12px; color:#E04e4e;">FITPASS</span>
            </td>
        </tr>
        <tr>
            <td style="background-color:#FBFBFB;">
                <table cellpadding="0" cellspacing="0" style="font-size:12px;border-radius:0 3px 3px 0; width:400px;">
                    <tbody>
                    <tr>
                        <td width="65" style="padding:20px 0 0 10px;vertical-align:top">
                            <img src="https://fitpass-images.s3.amazonaws.com/gallery_image_contact_ic_4EB5.png" width="40" height="40" style="width:40px">
                        </td>
                        <td width="333" style="padding:20px 0 20px 0">
                            <h3 style="margin:0;font-size:14px; color:#222222;">Get in touch with us!</h3>
                            <p style="margin:5px 0">
                                <a href="mailto:care@fitpass.co.in" style="font-weight:400;text-decoration:none;color:#222222">care@fitpass.co.in</a>
                                &nbsp; | &nbsp;
                                <a href="tel:011-46061468" style="font-weight:400;text-decoration:none;color:#222222">011-46061468</a>
                            </p></td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td style="background-color:#ffffff; padding:10px 10px 20px 10px;" align="center">
                <table cellpadding="0" cellspacing="0" style="width:100%;font-size:12px">
                    <tbody>
                    <tr>
                        <td colspan="2" style="font-weight:500; color:#10314f; padding:5px 0; font-size:14px; text-align:center; text-transform:uppercase;">
                            Download the FITPASS partner app!
                        </td>
                    </tr>
                    <tr>
                        <td align="right" style="font-weight:400; color:#2c485b; padding-right:10px;">
                            <a href="https://fitpass.co.in/partner-app" style="text-decoration:none;"><img src="https://fitpass-images.s3.amazonaws.com/gallery_image_app_store_ic_9AA3.png" width="99" height="30"/></a>
                        </td>
                        <td align="left" style="font-weight:500;color:#0e314e; padding-left:10px;">
                            <a href="https://fitpass.co.in/partner-app" style="text-decoration:none;">
                                <img src="https://fitpass-images.s3.amazonaws.com/gallery_image_play_store_AFCC.png" width="99" height="30"/></a>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td style="padding:0px 20px 20px 20px; font-family: 'Roboto', sans-serif; text-align:center; font-weight:400; font-size:10px; color:#777777;">You are receiving this email because you are one of our members and have agreed
                to sign up for our services.</td>
        </tr>
        </tbody>
    </table>
</div>
</body>
</html>