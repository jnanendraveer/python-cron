
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
    </head>

    <body>
        <div>
            <table width="100%"  style="background-color:rgb(243,243,243)" border="0" align="center" cellpadding="0" cellspacing="0">
                <tbody>
                    <tr>
                        <td align="center" style="padding-top:10px; padding-bottom:10px;"><table align="center" width="600" border="0" cellspacing="0" cellpadding="0">
                                <tbody>
                                    <tr>
                                        <td align="center" style="background-color:#15499c;padding-top:15px; padding-bottom:15px"><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                                                <tbody>
                                                    <tr>
                                                        <td width="170" align="left"><a href="https://fitpass.co.in" target="_blank"><img src="https://fitpass-images.s3.amazonaws.com/gallery_image_fp_logo_2ECB.png" width="170" alt="FITPASS"></a></td>
                                                        <td width="175">&nbsp;</td>
                                                        <td width="25"><img src="https://fitpass-images.s3.amazonaws.com/gallery_image_mail_ic_C6B3.png" width="25" height="25"  title="Welcome to FITPASS"/></td>
                                                        <td width="140" style="font-size:16px;font-family: 'Roboto', sans-serif;color:#FFF;text-align:right;line-height:22px;"><a href="mailto:care@fitpass.co.in" target="_blank" style="text-decoration:none; color:#FFF;">care@fitpass.co.in</a></td>
                                                    </tr>
                                                </tbody>
                                            </table></td>
                                    </tr>
                                    <tr>
                                        <td align="center" style="background-color:#FFF; padding-top:20px;"><div>
                                                <table width="90%" border="0" cellspacing="0" cellpadding="0">
                                                    <tbody>
                                                        <tr>
                                                            <td style="font-size:16px;font-family: 'Roboto', sans-serif;color:rgb(51,51,51);text-align:left;line-height:30px; padding-bottom:10px;">Dear <?php echo ucwords(strtolower($arrayData['user_name'])); ?>, </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="font-size:16px;font-family: 'Roboto', sans-serif;font-style:italic; text-align:left;line-height:30px;">Your membership is now active. You can now consult your personal nutritionist on the FITPASS app.</td>
                                                        </tr>

                                                    </tbody>
                                                </table>
                                            </div></td>
                                    </tr>
                                    <tr>
                                        <td align="center" style=";background-color:white;padding-bottom:20px"><table width="90%" border="0" bgcolor="#8d8f9c" style="font-size:14px; font-family: 'Roboto', sans-serif; color:#FFF; font-weight:300; padding:5px 0 5px 0;">
                                                <tr>
                                                    <th scope="col" style="font-size:16px; font-family: 'Roboto', sans-serif; font-weight:400; text-align:left; padding-left:10px;">Membership Details</th>
                                                </tr>
                                            </table>
                                            <table width="90%" border="0" cellpadding="10" cellspacing="0" align="center" style="border:#999 solid 1px;">
                                                <tr>
                                                    <td scope="col" style="font-size:18px; font-family: 'Roboto', sans-serif; font-weight:400; line-height:24px; text-align:left; color:#424242;">FITPASS ID</td>
                                                    <td scope="col" style="font-size:18px; font-family: 'Roboto', sans-serif; font-weight:400; line-height:24px; text-align:left; color:#424242;"><?php echo $arrayData['membership_id']; ?></td>
                                                </tr>
                                                <tr>
                                                    <td style="font-size:18px; font-family: 'Roboto', sans-serif; font-weight:400; line-height:24px; text-align:left; color:#424242;">FITFEAST Cycle End</td>
                                                    <td style="font-size:18px; font-family: 'Roboto', sans-serif; font-weight:400; line-height:24px; text-align:left; color:#424242;"><?php echo $arrayData['diet_end_of_cycle']; ?></td>


                                                </tr>
                                            </table></td>
                                    </tr>
                                    <tr>
                                        <td bgcolor="#FFFFFF" align="center" style="padding-top:0px;padding-bottom:20px"><table width="90%" border="0" cellspacing="0" cellpadding="0">
                                                <tbody>
                                                   

                                                    <tr>
                                                        <td style="font-size:16px;font-family: 'Roboto', sans-serif;color:rgb(51,51,51);text-align:left;line-height:25px;">Happy Sweating!<br/>
                                                            <span style="font-size:16px;font-family: 'Roboto', sans-serif;color:rgb(35,32,31); font-weight:500;text-align:left;">Team FITPASS</span></td>
                                                    </tr>
                                                </tbody>
                                            </table></td>
                                    </tr>
                                    <tr>
                                        <td align="center" bgcolor="#FFFFFF"><div>
                                                <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                                                    <tbody>
                                                        <tr>
                                                            <td style="background-color:#15499c">
                                                                <table width="100%" border="0">
                                                                    <tr>
                                                                        <th scope="col" style="font-size:18px; font-family: 'Roboto', sans-serif; font-weight:400; color:white; line-height:26px; padding:5px;"><a href="https://fitpass.co.in/" style="text-decoration:none; color:#FFF;">FITPASS</a></th>
                                                                        <th style="color:#FFF;">|</th>
                                                                        <th scope="col" style="font-size:18px; font-family: 'Roboto', sans-serif; font-weight:400; color:white; line-height:26px; padding:5px;"><a href="https://fitpass.co.in/dietitians" style="text-decoration:none; color:#FFF;">FITFEAST</a></th>
                                                                        <th style="color:#FFF;">|</th>
                                                                        <th scope="col" style="font-size:18px; font-family: 'Roboto', sans-serif; font-weight:400; color:white; line-height:26px; padding:5px;"><a href="https://fitpass.co.in/fitcoach" style="text-decoration:none; color:#FFF;">FITCOACH</a></th>
                                                                        <th style="color:#FFF;">|</th>
                                                                        <th scope="col" style="font-size:18px; font-family: 'Roboto', sans-serif; font-weight:400; color:white; line-height:26px; padding:5px;"><a href="https://fitpass.co.in/fitshop" style="text-decoration:none; color:#FFF;">FITSHOP</a></th>
                                                                    </tr>
                                                                </table></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center" style="padding-top:15px;padding-bottom:15px;background-color:#183248"><table width="85%" border="0" align="center" cellpadding="0" cellspacing="0">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td style="font-size:18px; font-family: 'Roboto', sans-serif; font-weight:300; color:white; line-height:26px;text-align:center; padding-bottom:5px;"> Get fit with just a tap! <span style="font-size:18px; font-family: 'Roboto', sans-serif; font-weight:700; color:white; line-height:26px;text-align:center;">GET THE APP</span></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="center"><table width="260" border="0" style="padding-bottom:10px;">
                                                                                    <tr>
                                                                                        <th scope="col"><a href="https://goo.gl/3uv4Hc" target="new" style="text-decoration:none;"><img src="https://fitpass-images.s3.amazonaws.com/gallery_image_app_store_ic_FA0F.png" /></a></th>
                                                                                        <th scope="col"><a href="https://goo.gl/TAAYfW" target="new" style="text-decoration:none;"><img src="https://fitpass-images.s3.amazonaws.com/gallery_image_play_store_ic_1957.png" /></a></th>
                                                                                    </tr>
                                                                                </table></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td width="100%" align="center">
                                                                                <table width="220" border="0" style="padding-bottom:10px;">
                                                                                    <tr>
                                                                                        <th scope="col"><a href="https://www.facebook.com/fitpassindia/" target="new" style="text-decoration:none;"><img src="https://fitpass-images.s3.amazonaws.com/gallery_image_fb_ic_4CC9.png" /></a></th>
                                                                                        <th scope="col"><a href="https://www.instagram.com/fitpassindia/" target="new" style="text-decoration:none;"><img src="https://fitpass-images.s3.amazonaws.com/gallery_image_im_ic_F5E3.png" /></a></th>
                                                                                        <th scope="col"><a href="https://twitter.com/fitpassindia/" target="new" style="text-decoration:none;"><img src="https://fitpass-images.s3.amazonaws.com/gallery_image_tw_ic_D2B6.png" /></a></th>
                                                                                        <th scope="col"><a href="https://fitpass.co.in/" target="new" style="text-decoration:none;"><img src="https://fitpass-images.s3.amazonaws.com/gallery_image_web_ic_53AF.png" /></a></th>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2" align="center"><img src="https://fitpass-images.s3.amazonaws.com/gallery_image_line_hr_54A2.png" /></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2" align="center" style="font-size:14px; font-family: 'Roboto', sans-serif; font-weight:300; color:white; padding-top:5px; line-height:26px; letter-spacing:2px;"><a href="mailto:care@fitpass.co.in" target="_blank" style="text-decoration:none; color:#FFF;">care@fitpass.co.in</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   |   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="tel:01146061468" target="_blank" style="text-decoration:none; color:#FFF;">011-46061468</td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div></td>
                                    </tr>
                                </tbody>
                            </table></td>
                    </tr>
                </tbody>
            </table>
    </body>
</html>

