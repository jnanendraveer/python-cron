<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
    </head>
    <body>
        <table width="100%" style="background-color:rgb(243,243,243)" border="0" align="center" cellpadding="0" cellspacing="0">
            <tbody>
                <tr>
                    <td align="center" style="padding-top:10px; padding-bottom:10px;"><table bgcolor="#FFFFFF" align="center" width="600" border="0" cellspacing="0" cellpadding="0">
                            <tbody>
                                <tr>
                                    <td align="center" style="background-color:#15499c;padding-top:15px; padding-bottom:15px"><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                                            <tbody>
                                                <tr>
                                                    <td width="170" align="left"><a href="https://fitpass.co.in" target="_blank">
                                                            <img src="https://fitpass-images.s3.amazonaws.com/gallery_image_fp_logo_2ECB.png" width="170" alt="FITPASS"/></a></td>
                                                    <td width="175">&nbsp;</td>
                                                    <td width="25"><img src="https://fitpass-images.s3.amazonaws.com/gallery_image_mail_ic_C6B3.png" width="25" height="25" />
                                                    </td>
                                                    <td width="140" style="font-size:16px;font-family: 'Roboto', sans-serif;color:#FFF;text-align:right;line-height:22px;">
                                                        <a href="mailto:care@fitpass.co.in" target="_blank" style="text-decoration:none; color:#FFF;">care@fitpass.co.in</a></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
