
<html xmlns="http://www.w3.org/1999/xhtml">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet" />

    </head>

    <body data-gramm="true" data-gramm_editor="true" data-gramm_id="b240b85e-9a4a-44d6-5a76-7aac5194cdd0" leftmargin="0" marginheight="0" marginwidth="0" offset="0" topmargin="0">
        <table bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
            <tbody>
                <tr>
                    <td align="center" valign="top">
                        <!-- BEGIN TEMPLATE // -->
                        <table bgcolor="#F7F7F7" border="0" cellpadding="0" cellspacing="0" style="width:600px; border:1px solid #F7F7F7;" width="600">
                            <tbody>
                                <tr>
                                    <td align="center" valign="top"><a href="https://fitpass.co.in/missoutfitness"><img src="https://marketing-image-production.s3.amazonaws.com/uploads/ea4fc1d514c68596b86b2176d6716dcbb8a894ab2e9141e797739918e239dc25341f153b22a0b1e0fb071d9870deee61de5f75d77dc3aebb232064a414bdcca4.png" /></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" valign="top">

                                        <table border="0" cellpadding="0" cellspacing="0" width="90%" style="padding-bottom:0px;">

                                            <tr>
                                                <td valign="top" style="font-family:'Roboto', sans-serif; font-weight:500; font-size:16px; line-height:28px; color:#071e34; padding-top:30px;  text-align:left;">Dear <?php echo ucwords($arrayData['username']);?></td>
                                            </tr>
                                            <tr>
                                                <td valign="top" style="font-family:'Roboto', sans-serif; font-weight:600; font-size:16px; line-height:28px; padding-bottom:10px; padding-top:10px; color:#ef4b4e; text-align:left;">

                                                    <p>Hope you enjoyed your diet membership.</p>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" style="font-family:'Roboto', sans-serif; font-weight:400; font-size:16px; line-height:28px; padding-bottom:20px; color:#071e34; text-align:left;">

                                                    <p>We would love to hear about your experience as a FITPASS diet membership user. Kindly spare 2 minutes of your time and fill up a quick feedback form (link below). This feedback will help us improve the quality of our service to provide you better and uncompromised solutions in the future.</p>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" align="center" style="margin:0 auto; padding-bottom:40px; padding-top:20px;">

                                                    <a href="https://fitpass.co.in/survey/step1?surveyid=hjgfj3874y345b34h587y&userid=<?php echo$arrayData['userId'];?>" style="text-decoration:none; color:#fff;"><span style="background-color:#ef4b4e; border-radius:8px; font-family:'Roboto', sans-serif; font-weight:600; font-size:16px; letter-spacing:1px; padding:12px; padding-left:30px; padding-right:30px; color:#ffffff; text-align:center; text-transform:uppercase">View Form</span></a>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" style="font-family:'Roboto', sans-serif; font-weight:400; font-size:16px; line-height:20px; padding-bottom:20px; color:#071e34; text-align:left;">

                                                    <p>Thanks <br>
                                                            Best Regards,<br/>
                                                        Team FITPASS
                                                    </p>

                                                </td>
                                            </tr>
                                        </table>

                                    </td>
                                </tr>
                                <tr>
                                    <td bgcolor="#071e34" style="font-family: 'Roboto', sans-serif; font-weight:300; font-size:22px; color:#ffffff;text-align:center; padding-top:20px;">GET THE FITPASS APP</td>
                                </tr>
                                <tr>
                                    <td align="center" bgcolor="#071e34" style="margin:0 auto; padding-bottom:10px; padding-top:5px;">
                                        <table border="0" cellspacing="0" width="250">
                                            <tbody>
                                                <tr>
                                                    <td align="center"><span class="sg-image" data-imagelibrary="%7B%22width%22%3A%22100%22%2C%22height%22%3A%2230%22%2C%22alignment%22%3A%22%22%2C%22src%22%3A%22https%3A//marketing-image-production.s3.amazonaws.com/uploads/fd6096d683f0da112522351ca23a0d040f963cd7442f183285dd6c909acea17c77147923ef5ee11b45b53c25dcf4e7780201d3518ca79941afc86f5e4de3f9ea.png%22%2C%22link%22%3A%22https%3A//itunes.apple.com/in/app/fitpass-health-fitness-pass/id1049745078%3Fmt%3D8%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%2C%22alt_text%22%3A%22App%20Store%22%7D"><a href="https://itunes.apple.com/in/app/fitpass-health-fitness-pass/id1049745078?mt=8" style="text-decoration:none;" target="_blank"><img alt="App Store" height="30" src="https://marketing-image-production.s3.amazonaws.com/uploads/fd6096d683f0da112522351ca23a0d040f963cd7442f183285dd6c909acea17c77147923ef5ee11b45b53c25dcf4e7780201d3518ca79941afc86f5e4de3f9ea.png" style="width: 100px; height: 30px;" width="100" /></a></span></td>
                                                    <td align="center"><span class="sg-image" data-imagelibrary="%7B%22width%22%3A%22100%22%2C%22height%22%3A%2230%22%2C%22alignment%22%3A%22%22%2C%22src%22%3A%22https%3A//marketing-image-production.s3.amazonaws.com/uploads/6ad805debbd519ff2ab8033d5df2825dff3aea0b5d012f8405adfb5f7d34382f72ba6e05e86c1e08a58023c8c6bc66267bc6dfde3167cac1be45097d491d5ce0.png%22%2C%22link%22%3A%22https%3A//play.google.com/store/apps/details%3Fid%3Dcom.india.fitpass%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%2C%22alt_text%22%3A%22Play%20store%22%7D"><a href="https://play.google.com/store/apps/details?id=com.india.fitpass" style="text-decoration:none;" target="_blank"><img alt="Play store" height="30" src="https://marketing-image-production.s3.amazonaws.com/uploads/6ad805debbd519ff2ab8033d5df2825dff3aea0b5d012f8405adfb5f7d34382f72ba6e05e86c1e08a58023c8c6bc66267bc6dfde3167cac1be45097d491d5ce0.png" style="width: 100px; height: 30px;" width="100" /></a></span></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" bgcolor="#071e34" style="padding-top:4px; padding-bottom:5px;" valign="top">
                                        <table border="0" cellspacing="0" width="200">
                                            <tbody>
                                                <tr>
                                                    <td align="center"><span class="sg-image" data-imagelibrary="%7B%22width%22%3A%2240%22%2C%22height%22%3A%2240%22%2C%22alignment%22%3A%22%22%2C%22src%22%3A%22https%3A//marketing-image-production.s3.amazonaws.com/uploads/e1407f89ef07a5830913cbf263da1bd1aa743e63f6ab54b7df86fe81e6da4de5f9f2b86c08959791825cb3ddece38d1ee5dc4bb0ec0683e485d49569114b1c55.png%22%2C%22link%22%3A%22%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%2C%22alt_text%22%3A%22%22%7D"><a href="https://www.facebook.com/fitpassindia" target="_blank"><img height="40" src="https://marketing-image-production.s3.amazonaws.com/uploads/e1407f89ef07a5830913cbf263da1bd1aa743e63f6ab54b7df86fe81e6da4de5f9f2b86c08959791825cb3ddece38d1ee5dc4bb0ec0683e485d49569114b1c55.png" style="width: 40px; height: 40px;" width="40" /></a></span></td>
                                                    <td align="center"><span class="sg-image" data-imagelibrary="%7B%22width%22%3A%2240%22%2C%22height%22%3A%2240%22%2C%22alignment%22%3A%22%22%2C%22src%22%3A%22https%3A//marketing-image-production.s3.amazonaws.com/uploads/ff4ab82b5ce94e420b1a7206cd7091481d96527c929a4c3c7e4a3330cc6ec4ecf44f0b0b71e0e65ae41ce5d2d0463f6fde1a2fcd99c819b52ab32e074749f317.png%22%2C%22link%22%3A%22%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%2C%22alt_text%22%3A%22%22%7D"><a href="https://twitter.com/fitpassindia" target="_blank"><img height="40" src="https://marketing-image-production.s3.amazonaws.com/uploads/ff4ab82b5ce94e420b1a7206cd7091481d96527c929a4c3c7e4a3330cc6ec4ecf44f0b0b71e0e65ae41ce5d2d0463f6fde1a2fcd99c819b52ab32e074749f317.png" style="width: 40px; height: 40px;" width="40" /></a></span></td>
                                                    <td align="center"><span class="sg-image" data-imagelibrary="%7B%22width%22%3A%2240%22%2C%22height%22%3A%2240%22%2C%22alignment%22%3A%22%22%2C%22src%22%3A%22https%3A//marketing-image-production.s3.amazonaws.com/uploads/380c10f6e29f82a85c0ad2419ff12f1b089bb6d37bd43fc59c23d3f5c5c830d6b877572a04a832d29415b84907515f43fe9a202c0d2db107d4499e721243ad75.png%22%2C%22link%22%3A%22%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%2C%22alt_text%22%3A%22%22%7D"><a href="https://www.instagram.com/fitpassindia/" target="_blank"><img height="40" src="https://marketing-image-production.s3.amazonaws.com/uploads/380c10f6e29f82a85c0ad2419ff12f1b089bb6d37bd43fc59c23d3f5c5c830d6b877572a04a832d29415b84907515f43fe9a202c0d2db107d4499e721243ad75.png" style="width: 40px; height: 40px;" width="40" /></a></span></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" bgcolor="#071e34">
                                        <table bgcolor="#071e34" border="0" cellspacing="0" width="300">
                                            <tbody>
                                                <tr>
                                                    <td align="center" style="font-family: 'Roboto', sans-serif; font-weight:400; font-size:14px; color:#ffffff; padding:10px;"><a href="mailto:care@fitpass.co.in?Subject=Team%20Fitpass" style="font-family: 'Roboto', sans-serif; font-weight:400; font-size:14px; color:#ffffff; text-decoration:none;" target="_top">care@fitpass.co.in</a>&nbsp;&nbsp; | &nbsp;&nbsp;
                                                        <a href="tel:01146061468" style="font-family: 'Roboto', sans-serif; font-weight:400; font-size:14px; color:#ffffff; text-decoration:none;">011-46061468</a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" style="font-family:'Roboto', sans-serif; font-weight:400; font-size:2px; color:#2e7dbb; padding:2px;"><a href="[Unsubscribe]" style="font-family: 'Roboto', sans-serif; font-weight:300; font-size:2px; color:#071e34; text-decoration:none;" target="_top">GET. SET. SWEAT.</a></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <!-- // END TEMPLATE -->
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </body>

</html>
