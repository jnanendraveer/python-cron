<!DOCTYPE html>
<html lang="en">
    <head>
        
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
        <style type="text/css">
            /* CLIENT-SPECIFIC STYLES */
            #outlook a{padding:0;} /* Force Outlook to provide a "view in browser" message */
            .ReadMsgBody{width:100%;} .ExternalClass{width:100%;} /* Force Hotmail to display emails at full width */
            .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;} /* Force Hotmail to display normal line spacing */
            body, table, td, a{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;} /* Prevent WebKit and Windows mobile changing default text sizes */
            table, td{mso-table-lspace:0pt; mso-table-rspace:0pt;} /* Remove spacing between tables in Outlook 2007 and up */
            img{-ms-interpolation-mode:bicubic;} /* Allow smoother rendering of resized image in Internet Explorer */

            /* RESET STYLES */
            body{margin:0; padding:0;}
            img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}
            table{border-collapse:collapse !important;}
            body{height:100% !important; margin:0; padding:0; width:100% !important;}

            /* iOS BLUE LINKS */
            .appleBody a {color:#68440a; text-decoration: none;}
            .appleFooter a {color:#999999; text-decoration: none;}

            /* MOBILE STYLES */
            @media screen and (max-width: 640px) {

                /* ALLOWS FOR FLUID TABLES */
                table[class="wrapper"]{
                    width:100% !important;
                }		

                .separator{
                    padding: 15px 0px !important;
                }

                table ul li{
                    text-align: center;
                }

                .map-location{
                    margin-top: 20px;
                    display: block;
                }


                /* ADJUSTS LAYOUT OF LOGO IMAGE */
                td[class="logo"]{
                    padding: 20px 0 20px 0 !important;
                }

                td[class="logo"] img{
                    margin:0 auto!important;
                }

                /* USE THESE CLASSES TO HIDE CONTENT ON MOBILE */
                td[class="mobile-hide"]{
                    display:none;}

                img[class="mobile-hide"]{
                    display: none !important;
                }

                img[class="img-max"]{
                    max-width: 100% !important;
                    width: 100% !important;
                    height:auto !important;
                }

                /* FULL-WIDTH TABLES */
                table[class="responsive-table"]{
                    width:100%!important;
                }

                /* UTILITY CLASSES FOR ADJUSTING PADDING ON MOBILE */
                td[class="padding"]{
                    padding: 10px 5% 15px 5% !important;
                }

                td[class="padding-copy"]{
                    padding: 0!important;
                    text-align: center;
                }

                td[class="padding-meta"]{
                    padding: 30px 5% 0px 5% !important;
                    text-align: center;
                }

                td[class="no-pad"]{
                    padding: 0 0 20px 0 !important;
                }

                td[class="no-padding"]{
                    padding: 0 !important;
                }

                td[class="section-padding"]{
                    padding: 10px 15px 10px 15px !important;
                }

                td[class="section-padding-bottom-image"]{
                    padding: 50px 15px 0 15px !important;
                }

                /* ADJUST BUTTONS ON MOBILE */
                td[class="mobile-wrapper"]{
                    padding: 0!important;
                }

                table[class="mobile-button-container"]{
                    margin:0 auto;
                    width:100% !important;
                }

                a[class="mobile-button"]{
                    width:80% !important;
                    padding: 15px !important;
                    border: 0 !important;
                    font-size: 16px !important;
                }

                .connect-us-wrapper{
                    padding: 10px 0!important
                }

                .connect-us{
                    display: block;
                    margin: 0 auto;
                    width: 80px;
                }

                .site-contact{
                    text-align: center!important;
                    float: none!important;
                    padding: 10px 20px 20px 0px!important;
                }

                .site-link{
                    padding: 0!important;

                }

                .main-box{
                    border: none!important;
                }

                .bullet-points{
                    text-align: left;

                }

                p{
                    font-size: 12px!important;
                }

                h2{
                    font-size: 20px!important;
                }

                h3{
                    font-size: 16px!important;
                }		

                .mailer-cta{
                    display: block;
                }

                .community-part{
                    padding: 0!important;
                }

                .social-icons{
                    margin-top: 4px!important;
                }

            }
        </style>
    </head>

    <body style="margin: 0; padding: 0;">
        <table border="0" cellpadding="0" cellspacing="0" style="margin: 0 auto; border: 1px solid #aeaeae;" class="main-box">
            <!-- HEADER -->
            <tr>
                <td bgcolor="#ffffff" align="center" style="padding: 0 0 10px;" class="section-padding">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="responsive-table">
                        <tr>
                            <td>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td class="padding-copy">
                                            <a href="http://fitpass.co.in/products/" target="_blank">
                                                <img src="<?php echo AWS_EMAIL_URL; ?>order-shipped-banner.jpg" width="630px" height="220" border="0" class="img-max">
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

            <!-- BODY CONTENT -->
            <tr>
                <td bgcolor="#ffffff" align="center" style="padding: 0;" class="section-padding">
                    <table border="0" cellpadding="0" cellspacing="0" width="600" class="responsive-table">
                        <tr>
                            <td>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td class="padding-copy" style="font-family: 'Roboto', sans-serif;">
                                            <p style="font-size: 14px; padding-bottom: 20px; line-height: 20px; margin: 0;color: #444444; line-height: 25px;">Dear <?php echo $arrayData['user_name'];
?>,
                                                <br/>Greetings from FITPASS!</p>
                                            <p style="font-size: 14px; padding-bottom: 20px; line-height: 20px; margin: 0;color: #444444; line-height: 25px;">The following products from your order have been shipped :  </p>
                                            <p style="font-weight: bold; margin: 15px 0; background: #f8f8f8; padding: 5px 5px 5px 10px; font-size: 14px; line-height: 20px; margin: 0;color: #444444; line-height: 25px;">Order ID: #<?php echo time() . '-' . $arrayData['productOrderLists']['order_list_id']; ?> |  Item(s) inside this consignment are:</p>
                                            <?php if (!empty($arrayData['productOrderLists']['tracking_id'])) { ?>
                                                <p style=" margin: 15px 0; background: #f8f8f8; padding: 5px 5px 5px 10px; font-size: 14px; line-height: 20px; margin: 0;color: #444444; line-height: 25px;">Track your shipment <a href="http://<?php echo $arrayData['productOrderLists']['tracking_url']; ?>" target="_blank">Click Here</a></p>
                                                <p style="margin: 15px 0; background: #f8f8f8; padding: 5px 5px 5px 10px; font-size: 14px; line-height: 20px; margin: 0;color: #444444; line-height: 25px;">Tracking Id:<?php echo $arrayData['productOrderLists']['tracking_id']; ?> </p>
                                            <?php } ?> 
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr>
                <td bgcolor="#ffffff" align="center" style="padding:10px 0 10px 0;" class="section-padding">
                    <table border="0" cellpadding="0" cellspacing="0" width="600" class="responsive-table">
                        <tr>
                            <td>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td class="padding-copy" style="font-family: 'Roboto', sans-serif;">
                                            <table  width="100%" border="0" cellspacing="0" cellpadding="0" width="600">
                                                <tr>
                                                    <td style="width: 33%; border-bottom: 1px solid #e8e8e8; padding: 5px; font-size: 14px; line-height: 20px; margin: 0;color: #444444; line-height: 25px;">
                                                        <strong>Product Name</strong>
                                                    </td>
                                                    <td style="width: 33%; border-bottom: 1px solid #e8e8e8; ">&nbsp;</td>
                                                    <td style="width: 33%; border-bottom: 1px solid #e8e8e8; text-align: center; padding: 5px; font-size: 14px; line-height: 20px; margin: 0;color: #444444; line-height: 25px;">
                                                        <strong>Quantity</strong>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 33%; border-bottom: 1px solid #e8e8e8; padding: 5px; font-size: 14px; line-height: 20px; margin: 0;color: #444444; line-height: 25px;">
                                                        <a target="_blank" href="http://fitpass.co.in/product/<?php echo ucwords(strtolower($arrayData['productModel']['product_seo_url'])); ?>" style="color: #20cfb1; text-decoration: none;"><?php echo ucwords(strtolower($arrayData['productModel']['product_name'])); ?></a>
                                                    </td>
                                                    <td style="width: 33%; border-bottom: 1px solid #e8e8e8; ">&nbsp;</td>
                                                    <td style="width: 33%; border-bottom: 1px solid #e8e8e8; text-align: center; padding: 5px; font-size: 14px; line-height: 20px; margin: 0;color: #444444; line-height: 25px;">
                                                        <?php echo $arrayData['productOrderLists']['product_quantity']; ?>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr>
                <td bgcolor="#ffffff" align="center" style="padding:10px 0 20px 0;" class="section-padding">
                    <table border="0" cellpadding="0" cellspacing="0" width="600" class="responsive-table">
                        <tr>
                            <td>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td class="padding-copy" style="font-family: 'Roboto', sans-serif;">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" width="600" >
                                                <tr>
                                                    <td colspan="2">
                                                        <p style="padding-bottom: 10px; font-size: 14px; line-height: 20px; margin: 0;color: #444444;">We sincerely hope that you are satisfied with our service. <br/>As a constant endeavour to improve our services,  we request you to share
                                                            some feedback on your shopping experience with FITPASS.</p>
                                                        <p style="padding-top: 10px; padding-bottom: 20px; font-size: 14px; line-height: 20px; margin: 0;color: #444444;">Please email us at <span style="color: #20cfb1">care@fitpass.co.in</span> in case of any questions or further assistance.</p>
                                                        <p style="padding-bottom: 10px; font-size: 14px; line-height: 20px; margin: 0;color: #444444;">Thanks for shopping with us!</p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <p style="font-size: 13px;  margin: 10px 0 0 0; color: #444444; line-height: 18px;">Warm regards,<br/>Team FITPASS </p>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>

        <table border="0" cellpadding="0" cellspacing="0" style="margin: 0 auto;" width="100%">
            <td bgcolor="#ffffff" align="center" style="padding: 0 0;" class="section-padding">
                <table border="0" cellpadding="0" cellspacing="0" width="630" class="responsive-table">		
                    <tr>
                        <!-- GET THE APP -->
                        <td style="background: #000; padding: 15px 0 15px 0; font-family: 'Roboto', sans-serif;  margin: 10px 0; color: #fff; font-size: 13px; text-align: center;">
                            <div class="getthefitpass-app" style="width: 270px; margin:0 auto">
                                <span style="margin-right: 10px; float: left; padding-top: 20px">GET THE FIT<span style="color:#20cfb1;">PASS</span> APP <img src="<?php echo AWS_EMAIL_URL; ?>arrow.jpg" valign="middle"/></span>			
                                <span style="float: right">
                                    <a href="https://itunes.apple.com/nz/app/fitpass/id1049745078?mt=8" target="_blank"><img src="<?php echo AWS_EMAIL_URL; ?>apple.jpg" valign="middle"/></a>
                                    <a href="https://play.google.com/store/apps/details?id=com.india.fitpass" target="_blank"><img src="<?php echo AWS_EMAIL_URL; ?>android.jpg" valign="middle"/></a>
                                </span>
                            </div>
                        </td>
                    </tr>

                    <tr>

                        <td>
                            <!-- FOOTER -->
                            <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                <tr>
                                    <td valign="top" style="padding: 0;" class="mobile-wrapper">
                                        <!-- CONNECT US -->
                                        <table cellpadding="0" cellspacing="0" border="0" width="33%" align="left" class="responsive-table">
                                            <tr>
                                                <td style="font-size: 11px; font-family: 'Roboto', sans-serif; color: #444; padding: 20px 0 20px 20px;" class="connect-us-wrapper">	
                                                    <div class="connect-us">
                                                        <span style="float:left">Connect with us</span>
                                                        <div class="social-icons" style="float:left; margin:-5px 0 0 5px;">
                                                            <a href="https://www.facebook.com/fitpassindia" target="_blank"><img src="<?php echo AWS_EMAIL_URL; ?>facebook.jpg" valign="middle"/></a>
                                                            <a href="https://www.instagram.com/fitpassindia/" target="_blank"><img src="<?php echo AWS_EMAIL_URL; ?>instagram.jpg" valign="middle"/></a>
                                                            <a href="https://twitter.com/fitpassindia" target="_blank"><img src="<?php echo AWS_EMAIL_URL; ?>twitter.jpg" valign="middle"/></a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>

                                        <!-- SITE LINK -->
                                        <table cellpadding="0" cellspacing="0" border="0" width="33%" align="left" class="responsive-table">
                                            <tr>
                                                <td style="font-size: 11px; font-family: 'Roboto', sans-serif; text-align: center; color: #444; padding: 20px 0 0 0;" class="site-link">
                                                    <a href="http://fitpass.co.in/" target="_blank" style="text-decoration: none; color: #444;">www.fitpass.co.in</a>
                                                </td>
                                            </tr>
                                        </table>

                                        <!-- SITE CONTACT -->
                                        <table cellpadding="0" cellspacing="0" border="0" width="33%" align="left" class="responsive-table">
                                            <tr>
                                                <td style="font-size: 11px; color: #444; float: right; font-family: 'Roboto', sans-serif; text-align: right; padding: 20px 20px 20px 0;" class="site-contact">
                                                    Email us at: care@fitpass.co.in<br/>011-46061468

                                                </td>
                                            </tr>
                                        </table>

                                    </td>								
                                </tr>
                            </table>
                        </td>
                    </tr>	
                </table>
            </td>
        </tr>
    </table>

</body>
</html>
