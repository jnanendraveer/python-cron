
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
    </head>
    <body>
        <div>
            <table width="100%" style="background-color:rgb(243,243,243)" border="0" align="center" cellpadding="0" cellspacing="0">
                <tbody>
                    <tr>
                        <td align="center" style="padding-top:10px; padding-bottom:10px;"><table align="center" width="600" border="0" cellspacing="0" cellpadding="0">
                                <tbody>
                                    <tr>
                                        <td align="center" style="background-color:#15499c;padding-top:15px; padding-bottom:15px"><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                                                <tbody>
                                                    <tr>
                                                        <td width="170" align="left"><a href="https://fitpass.co.in" target="_blank"><img src="https://fitpass-images.s3.amazonaws.com/gallery_image_fp_logo_2ECB.png" width="170" alt="FITPASS"></a></td>
                                                        <td width="175">&nbsp;</td>
                                                        <td width="25"><img src="https://fitpass-images.s3.amazonaws.com/gallery_image_mail_ic_C6B3.png" width="25" height="25" /></td>
                                                        <td width="140" style="font-size:16px;font-family: 'Roboto', sans-serif;color:#FFF;text-align:right;line-height:22px;"><a href="mailto:care@fitpass.co.in" target="_blank" style="text-decoration:none; color:#FFF;">care@fitpass.co.in</a></td>
                                                    </tr>
                                                </tbody>
                                            </table></td>
                                    </tr>
                                    <tr>
                                        <td align="center" style="background-color:#FFF; padding-top:20px;"><div>
                                                <table width="95%" border="0" cellspacing="0" cellpadding="0">
                                                    <tbody>
                                                        <tr>
                                                            <td style="font-size:16px;font-family: 'Roboto', sans-serif;color:rgb(51,51,51);text-align:left;line-height:30px; padding-top:20px;">Dear <?php echo $arrayData['user_name']; ?>, </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="font-size:20px;font-family: 'Roboto', sans-serif;color:#00a550; font-weight:700; font-style:italic; text-align:left;line-height:30px; padding-bottom:10px">Continue your health streak!</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="font-size:16px;font-family: 'Roboto', sans-serif;color:rgb(51,51,51);text-align:left;line-height:22px; padding-bottom:20px;">Your FITFEAST membership will expire in 6 days.</td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center" style="padding-top:10px; padding-bottom:10px;"><div style="width:200px; border-radius:8px; background:#d9e3e8; padding:10px; font-size:30px;font-family: 'Roboto', sans-serif;color:#ef4a4e; font-weight:700; vertical-align:middle;"><span style="vertical-align:middle; padding-right:10px;"><img src="https://fitpass-images.s3.amazonaws.com/gallery_image_calender_ic_DD31.png" width="36" height="40"></span> <?php echo date('d-M', strtotime($arrayData['diet_end_of_cycle'])) ?></div></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center" style="padding-top:5px; padding-bottom:20px;">
                                                                <a href="http://fitpass.co.in/fitfeast/plandetails?https://d.fitpass.co.in/d2tuTg9WTN" target="new" style="text-decoration:none; color:#FFF;">
                                                                    <div style="width:160px; border-radius:8px; background:#00a54e; padding:10px; font-size:20px;font-family: 'Roboto', sans-serif;color:#ffffff; letter-spacing:1px; font-weight:700;"> RENEW NOW</div>
                                                                </a></td>
                                                        </tr>

                                                        <tr>
                                                            <td style="font-size:16px;font-family: 'Roboto', sans-serif;color:rgb(51,51,51);text-align:left;line-height:25px; padding-bottom:10px;">Renew your membership and continue enjoying healthy foods and treats.</td>
                                                        </tr>

                                                    </tbody>
                                                </table>
                                            </div></td>
                                    </tr>
                                    <tr>
                                        <td align="center" bgcolor="#FFFFFF"><div>
                                                <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                                                    <tbody>
                                                      <tr>
                                                            <td style="background-color:#15499c"><table width="100%" border="0">
                                                                    <tr>
                                                                        <th scope="col" style="font-size:18px; font-family: 'Roboto', sans-serif; font-weight:400; color:white; line-height:26px; padding:5px;"><a href="https://fitpass.co.in/" style="text-decoration:none; color:#FFF;">FITPASS</a></th>
                                                                        <th style="color:#FFF;">|</th>
                                                                        <th scope="col" style="font-size:18px; font-family: 'Roboto', sans-serif; font-weight:400; color:white; line-height:26px; padding:5px;"><a href="https://fitpass.co.in/dietitians" style="text-decoration:none; color:#FFF;">FITFEAST</a></th>
                                                                        <th style="color:#FFF;">|</th>
                                                                        <th scope="col" style="font-size:18px; font-family: 'Roboto', sans-serif; font-weight:400; color:white; line-height:26px; padding:5px;"><a href="https://fitpass.co.in/fitcoach" style="text-decoration:none; color:#FFF;">FITCOACH</a></th>
                                                                        <th style="color:#FFF;">|</th>
                                                                        <th scope="col" style="font-size:18px; font-family: 'Roboto', sans-serif; font-weight:400; color:white; line-height:26px; padding:5px;"><a href="https://fitpass.co.in/fitshop" style="text-decoration:none; color:#FFF;">FITSHOP</a></th>
                                                                    </tr>
                                                                </table></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center" style="padding-top:15px;padding-bottom:15px;background-color:#4c5b63"><table width="85%" border="0" align="center" cellpadding="0" cellspacing="0">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td width="30%" align="center"><table width="220" border="0" style="padding-bottom:10px;">
                                                                                    <tr>
                                                                                        <th colspan="4" scope="col" style="font-size:18px; font-family: 'Roboto', sans-serif; font-weight:300; letter-spacing:2px; color:white; line-height:26px;text-align:center;">Connect with us!</th>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <th scope="col"><a href="https://www.facebook.com/fitpassindia/" target="new" style="text-decoration:none;"><img src="https://fitpass-images.s3.amazonaws.com/gallery_image_fb_ic_4CC9.png" /></a></th>
                                                                                        <th scope="col"><a href="https://www.instagram.com/fitpassindia/" target="new" style="text-decoration:none;"><img src="https://fitpass-images.s3.amazonaws.com/gallery_image_im_ic_F5E3.png" /></a></th>
                                                                                        <th scope="col"><a href="https://twitter.com/fitpassindia/" target="new" style="text-decoration:none;"><img src="https://fitpass-images.s3.amazonaws.com/gallery_image_tw_ic_D2B6.png" /></a></th>
                                                                                        <th scope="col"><a href="https://fitpass.co.in/" target="new" style="text-decoration:none;"><img src="https://fitpass-images.s3.amazonaws.com/gallery_image_web_ic_53AF.png" /></a></th>
                                                                                    </tr>
                                                                                </table></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2" align="center"><img src="https://fitpass-images.s3.amazonaws.com/gallery_image_line_hr_54A2.png" /></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2" align="center" style="font-size:14px; font-family: 'Roboto', sans-serif; font-weight:300; color:white; padding-top:5px; line-height:26px; letter-spacing:2px;"><a href="mailto:care@fitpass.co.in" target="_blank" style="text-decoration:none; color:#FFF;">care@fitpass.co.in</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   |   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="tel:01146061468" target="_blank" style="text-decoration:none; color:#FFF;">011-46061468</td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div></td>
                                    </tr>
                                </tbody>
                            </table></td>
                    </tr>
                </tbody>
            </table>
    </body>
</html>
