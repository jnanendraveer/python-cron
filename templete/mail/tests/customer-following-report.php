
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>FITPASS CRM</title>
	<link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
    </head>

    <body>	

	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	    <tbody>
		<tr>
		    <td align="center" style="background-color:rgb(243,243,243)"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
			    <tbody>
				<tr>
				    <td align="center"><table align="center" width="600" border="0" cellspacing="0" cellpadding="0">
					    <tbody>
						<tr>
						    <td align="center" style="padding-top:10px;padding-bottom:15px"><table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
							    <tbody>
								<tr>
								    <td style="font-size:12px;font-family: 'Roboto', sans-serif;color:rgb(105,105,105);text-align:center">&nbsp;
								    </td>
								</tr>
							    </tbody>
							</table></td>
						</tr>
						<tr>
						    <td class="m_-1729362336069348764gmail-m_-1268178667708316512padt10m m_-1729362336069348764gmail-m_-1268178667708316512padb10m" align="center" style="background-color:white;padding-top:15px;padding-bottom:15px"><table width="85%" border="0" align="center" cellpadding="0" cellspacing="0">
							    <tbody>
								<tr>
								    <td width="35" align="left"><a href="https:fitpass.co.in" target="_blank"><img src="http://images.fitpass.co.in/gallery_image_fp-logo_0777.png" width="170" alt="FITPASS" style="display:block;border:0px;font-size:20px;font-weight:bold;font-family:sans-serif;color:rgb(13,13,13)" class="m_-1729362336069348764gmail-m_-1268178667708316512resizem CToWUd"></a></td>
								</tr>
							    </tbody>
							</table></td>
						</tr>
						<tr>
						    <td align="center" style="background-size:100% 100%;background-position:50% 50%;background-repeat:repeat;background-color:rgb(23,42,75)"><div>
							    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
								<tbody>
								    <tr>
									<td align="center" style="padding-top:20px;padding-bottom:20px"><table width="85%" border="0" align="center" cellpadding="0" cellspacing="0">
										<tbody>
										    <tr>
											<td style="font-size:20px; font-family: 'Roboto', sans-serif;color:white;text-align:center; line-height:28px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse tempor erat vitae urna sagittis..</td>
										    </tr>
										    <tr>
											<td style="padding-top:25px"><table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tbody>
												    <tr>
													<td align="center" style="background-color:white;padding-top:20px"><table width="90%" border="0" cellspacing="0" cellpadding="0">
														<tbody>
														    <tr>
															<td style="font-size:16px;font-family: 'Roboto', sans-serif;color:rgb(51,51,51);text-align:left;line-height:30px; padding-bottom:20px;">Dear Partner,<br />
															    Please find the list of #leads exported from CRM software.<br>

															</td>
														    </tr>
														</tbody>
													    </table></td>
												    </tr>
												</tbody>
											    </table></td>
										    </tr>
										</tbody>
									    </table></td>
								    </tr>
								</tbody>
							    </table>
							</div></td>
						</tr>
						<tr>
						    <td align="center" style="border-top:2px solid rgb(217,217,217);background-color:white;padding-top:20px;padding-bottom:25px">
							<table width="90%" border="0" bgcolor="#8d8f9c" style="font-size:14px; font-family: 'Roboto', sans-serif; color:#FFF; font-weight:300; padding:5px 0 5px 0;">
							    <tr>
								<th scope="col" width="40">S.N.</th>
								<th scope="col" width="118">Name</th>
								<th scope="col" width="170">Mobile No.</th>
								<th scope="col" width="194" style="text-align:left; padding-left:50px;">E-mail ID</th>
							    </tr>
							</table>
							<table width="90%" border="0" cellpadding="5" cellspacing="0" style="border:#999 solid 1px; font-size:14px; font-family: 'Roboto', sans-serif; font-weight:300; color:#000000;">
							    <tr>
								<th scope="col" width="40" style="font-size:14px; font-family: 'Roboto', sans-serif; font-weight:400; text-align:center; color:#b7b7b7;">01.</th>
								<th scope="col" width="138" style="font-size:14px; font-family: 'Roboto', sans-serif; font-weight:400; text-align:left;">John Morrison</th>
								<th scope="col" width="132" style="font-size:14px; font-family: 'Roboto', sans-serif; font-weight:400; text-align:left;">+91-88775 58841</th>
								<th scope="col" width="211" style="font-size:14px; font-family: 'Roboto', sans-serif; font-weight:400; text-align:left; line-height:18px">john.morrison@mail.com<br/><span style="font-size:12px; font-family: 'Roboto', sans-serif; font-weight:400; color:#727272; text-align:left;"><em>Follow-up: 23 Mar, 2018</em></span></th>
							    </tr>
							    <tr bgcolor="#eaeaea">
								<th scope="col" width="40" style="font-size:14px; font-family: 'Roboto', sans-serif; font-weight:400; text-align:center; color:#b7b7b7;">02.</th>
								<th scope="col" width="138" style="font-size:14px; font-family: 'Roboto', sans-serif; font-weight:400; text-align:left;">John Morrison</th>
								<th scope="col" width="132" style="font-size:14px; font-family: 'Roboto', sans-serif; font-weight:400; text-align:left;">+91-88775 58841</th>
								<th scope="col" width="211" style="font-size:14px; font-family: 'Roboto', sans-serif; font-weight:400; text-align:left; line-height:18px">john.morrison@mail.com<br/><span style="font-size:12px; font-family: 'Roboto', sans-serif; font-weight:400; color:#727272; text-align:left;"><em>Follow-up: 23 Mar, 2018</em></span></th>
							    </tr>
							    <tr>
								<th scope="col" width="40" style="font-size:14px; font-family: 'Roboto', sans-serif; font-weight:400; text-align:center; color:#b7b7b7;">03.</th>
								<th scope="col" width="138" style="font-size:14px; font-family: 'Roboto', sans-serif; font-weight:400; text-align:left;">John Morrison</th>
								<th scope="col" width="132" style="font-size:14px; font-family: 'Roboto', sans-serif; font-weight:400; text-align:left;">+91-88775 58841</th>
								<th scope="col" width="211" style="font-size:14px; font-family: 'Roboto', sans-serif; font-weight:400; text-align:left; line-height:18px">john.morrison@mail.com<br/><span style="font-size:12px; font-family: 'Roboto', sans-serif; font-weight:400; color:#727272; text-align:left;"><em>Follow-up: 23 Mar, 2018</em></span></th>
							    </tr>
							    <tr bgcolor="#eaeaea">
								<th scope="col" width="40" style="font-size:14px; font-family: 'Roboto', sans-serif; font-weight:400; text-align:center; color:#b7b7b7;">04.</th>
								<th scope="col" width="138" style="font-size:14px; font-family: 'Roboto', sans-serif; font-weight:400; text-align:left;">John Morrison</th>
								<th scope="col" width="132" style="font-size:14px; font-family: 'Roboto', sans-serif; font-weight:400; text-align:left;">+91-88775 58841</th>
								<th scope="col" width="211" style="font-size:14px; font-family: 'Roboto', sans-serif; font-weight:400; text-align:left; line-height:18px">john.morrison@mail.com<br/><span style="font-size:12px; font-family: 'Roboto', sans-serif; font-weight:400; color:#727272; text-align:left;"><em>Follow-up: 23 Mar, 2018</em></span></th>
							    </tr>
							    <tr>
								<th scope="col" width="40" style="font-size:14px; font-family: 'Roboto', sans-serif; font-weight:400; text-align:center; color:#b7b7b7;">05.</th>
								<th scope="col" width="138" style="font-size:14px; font-family: 'Roboto', sans-serif; font-weight:400; text-align:left;">John Morrison</th>
								<th scope="col" width="132" style="font-size:14px; font-family: 'Roboto', sans-serif; font-weight:400; text-align:left;">+91-88775 58841</th>
								<th scope="col" width="211" style="font-size:14px; font-family: 'Roboto', sans-serif; font-weight:400; text-align:left; line-height:18px">john.morrison@mail.com<br/><span style="font-size:12px; font-family: 'Roboto', sans-serif; font-weight:400; color:#727272; text-align:left;"><em>Follow-up: 23 Mar, 2018</em></span></th>
							    </tr>

							</table>

						    </td>
						</tr>
						<tr>
						    <td align="center" bgcolor="#FFFFFF"><div>
							    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
								<tbody>
								    <tr>
									<td align="center" style="padding-top:5px;padding-bottom:25px"><a href="#" style="text-decoration:none; color:#ffffff;"><span style="border-radius:5px; font-size:16px; font-family: 'Roboto', sans-serif; font-weight:400; padding:10px; background-color:#00a550; color:#FFF;">View more...</span></a></td>
								    </tr>
								    <tr>
									<td class="m_-1729362336069348764gmail-m_-1268178667708316512overlay" align="center" style="padding-top:15px;padding-bottom:15px;background-color:#183248"><table width="85%" border="0" align="center" cellpadding="0" cellspacing="0">
										<tbody>
										    <tr><td><img src="http://images.fitpass.co.in/gallery_image_bt-logo_596A.png" /></td>
											<td style="font-size:14px; font-family: 'Roboto', sans-serif; font-weight:400; color:white; line-height:26px;text-align:left;">ASR Market Ventures Pvt. Ltd.<br/>
											    � 2018 fitpass.co.in | All Rights Reserved.</td>
										    </tr>
										</tbody>
									    </table></td>
								    </tr>
								</tbody>
							    </table>
							</div></td>
						</tr>
					    </tbody>
					</table></td>
				</tr>
			    </tbody>
			</table>

			<!-- footer section -->



		    </td>
		</tr>
	    </tbody>
	</table>
    </body>
</html>
