<tr>
    <td bgcolor="#FFFFFF" align="center"><table width="90%" border="0" cellspacing="0" cellpadding="0">
            <tbody>

                <tr>
                    <td style="font-size:16px;font-family: 'Roboto', sans-serif;color:rgb(51,51,51);text-align:left;line-height:25px; padding-top:10px; padding-bottom:20px;">Happy sweating!<br>
                        <span style="font-size:16px;font-family: 'Roboto', sans-serif;color:rgb(35,32,31); font-weight:500;text-align:left;">Team FITPASS</span></td>
                </tr>
            </tbody>
        </table></td>
</tr>
<tr>
    <td align="center"><div>
            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tbody>
                    <tr>
                        <td style="background-color:#15499c"><table width="100%" border="0">
                                <tr>
                                    <th scope="col" style="font-size:18px; font-family: 'Roboto', sans-serif; font-weight:400; color:white; line-height:26px; padding:5px;"><a href="https://fitpass.co.in/" style="text-decoration:none; color:#FFF;">FITPASS</a></th>
                                    <th style="color:#FFF;">|</th>
                                    <th scope="col" style="font-size:18px; font-family: 'Roboto', sans-serif; font-weight:400; color:white; line-height:26px; padding:5px;"><a href="https://fitpass.co.in/dietitians" style="text-decoration:none; color:#FFF;">FITFEAST</a></th>
                                    <th style="color:#FFF;">|</th>
                                    <th scope="col" style="font-size:18px; font-family: 'Roboto', sans-serif; font-weight:400; color:white; line-height:26px; padding:5px;"><a href="https://fitpass.co.in/fitcoach" style="text-decoration:none; color:#FFF;">FITCOACH</a></th>
                                    <th style="color:#FFF;">|</th>
                                    <th scope="col" style="font-size:18px; font-family: 'Roboto', sans-serif; font-weight:400; color:white; line-height:26px; padding:5px;"><a href="https://fitpass.co.in/fitshop" style="text-decoration:none; color:#FFF;">FITSHOP</a></th>
                                </tr>
                            </table></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="center" style="padding-top:15px;padding-bottom:15px;background-color:#4c5b63;">
                            <table width="85%" border="0" align="center" cellpadding="0" cellspacing="0">
                                <tbody>
                                    <tr>
                                        <td width="30%" align="center">
                                            <table width="220" border="0" style="padding-bottom:10px;">
                                                <tr>
                                                    <th colspan="4" scope="col" style="font-size:18px; font-family: 'Roboto', sans-serif; font-weight:300; letter-spacing:2px; color:white; line-height:26px;text-align:center;">Connect with us!</th>
                                                </tr>
                                                <tr>
                                                    <th scope="col"><a href="https://www.facebook.com/fitpassindia/" target="new" style="text-decoration:none;"><img src="https://fitpass-images.s3.amazonaws.com/gallery_image_fb_ic_4CC9.png" /></a></th>
                                                    <th scope="col"><a href="https://www.instagram.com/fitpassindia/" target="new" style="text-decoration:none;"><img src="https://fitpass-images.s3.amazonaws.com/gallery_image_im_ic_F5E3.png" /></a></th>
                                                    <th scope="col"><a href="https://twitter.com/fitpassindia/" target="new" style="text-decoration:none;"><img src="https://fitpass-images.s3.amazonaws.com/gallery_image_tw_ic_D2B6.png" /></a></th>
                                                    <th scope="col"><a href="https://fitpass.co.in/" target="new" style="text-decoration:none;"><img src="https://fitpass-images.s3.amazonaws.com/gallery_image_web_ic_53AF.png" /></a></th>
                                                </tr>
                                            </table></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="center"><img src="https://fitpass-images.s3.amazonaws.com/gallery_image_line_hr_54A2.png" /></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="center" style="font-size:14px; font-family: 'Roboto', sans-serif; font-weight:300; color:white; padding-top:5px; line-height:26px; letter-spacing:2px;"><a href="mailto:care@fitpass.co.in" target="_blank" style="text-decoration:none; color:#FFF;">care@fitpass.co.in</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   |   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="tel:01146061468" target="_blank" style="text-decoration:none; color:#FFF;">011-46061468</td>
                                    </tr>
                                </tbody>
                            </table></td>
                    </tr>
                </tbody>
            </table>
        </div></td>
</tr>
</tbody>
</table></td>
</tr>
</tbody>
</table>
</body>
</html>
