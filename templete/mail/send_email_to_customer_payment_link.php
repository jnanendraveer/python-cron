
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet"> 
    </head>

    <body>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td align="center" style="margin:0 auto;"><center style="font-family: 'Roboto', sans-serif;line-height:20px;color:#58666e;max-width:600px">
                        <table width="100%" height="100%" align="center" style="font-family: 'Roboto', sans-serif;line-height:20px;color:#58666e;width:100%;background-color:#fafafa" border="0" cellspacing="0" cellpadding="0">
                            <tbody style="font-family: 'Roboto', sans-serif;line-height:20px;color:#58666e">
                                <tr style="font-family: 'Roboto', sans-serif;line-height:20px;color:#58666e">
                                    <td style="font-family: 'Roboto', sans-serif;line-height:20px;padding:24px 4%;padding-bottom:0;border-left:1px solid #f2f2f2;width:3%;background-color:rgb(35,113,236);color:#ffffff"></td>
                                    <td style="font-family: 'Roboto', sans-serif;line-height:20px;padding-bottom:0;width:92%; background-image: linear-gradient(0deg, #303395, #1b9dff);color:#ffffff;padding:24px 0!important;border-left:0;border-right:0" colspan="2"><table width="100%" height="100%" style="font-family: 'Roboto', sans-serif;line-height:20px;color:#212121;width:100%" border="0" cellspacing="0" cellpadding="0">
                                            <tbody style="font-family: 'Roboto', sans-serif;line-height:20px;color:#212121">
                                                <tr style="font-family: 'Roboto', sans-serif;line-height:20px;color:#212121">
                                                    <td style="font-family: 'Roboto', sans-serif;line-height:20px;color:#212121;text-align:center"><div style="font-family: 'Roboto', sans-serif;line-height:20px;color:#212121"> <img style="font-family: 'Roboto', sans-serif;line-height:20px;color:#212121;width:180px;height:auto;margin-bottom:12px" src="https://fitpass-images.s3.amazonaws.com/gallery_image_fitpass_logo_4E3B.png"> </div></td>
                                                </tr>
                                                <tr style="font-family: 'Roboto', sans-serif;line-height:20px;color:#212121">
                                                    <td style="font-family: 'Roboto', sans-serif;line-height:20px;color:#212121;text-align:center"><h2 style="font-family: 'Roboto', sans-serif;margin:0;font-size:20px;line-height:24px;color:#ffffff"> Payment Link from FITPASS </h2></td>
                                                </tr>
                                                <tr style="font-family: 'Roboto', sans-serif;line-height:20px;color:#212121">
                                                    <td style="font-family: 'Roboto', sans-serif;line-height:20px;color:#212121;text-align:center"><div style="font-family: 'Roboto', sans-serif;line-height:20px;color:#ffffff"> Payment Link Id: #FTP<?php echo $arrayData['invoice_number'];?> </div></td>
                                                </tr>
                                                <tr style="font-family: 'Roboto', sans-serif;line-height:20px;color:#212121">
                                                    <td style="font-family: 'Roboto', sans-serif;line-height:20px;color:#212121;text-align:center"><div style="font-family: 'Roboto', sans-serif;line-height:20px;margin-top:12px;color:#ffffff">
                                                            <div style="font-family: 'Roboto', sans-serif;line-height:20px;color:#ffffff"> FITPASS has sent you a payment link for ₹<?php echo $arrayData['total_invoice_amount'];?> </div>
                                                        </div></td>
                                                </tr>
                                            </tbody>
                                        </table></td>
                                    <td style="font-family: 'Roboto', sans-serif;line-height:20px;padding:24px 4%;padding-bottom:0;border-right:1px solid #f2f2f2;width:3%;background-color:rgb(35,113,236);color:#ffffff"></td>
                                </tr>
                                <tr style="font-family: 'Roboto', sans-serif;line-height:20px;color:#58666e">
                                    <td style="font-family: 'Roboto', sans-serif;line-height:20px;padding:24px 4%;padding-bottom:0;border-left:1px solid #f2f2f2;width:3%;background-color:rgb(35,113,236);color:#ffffff"></td>
                                    <td style="font-family: 'Roboto', sans-serif;line-height:20px;color:#58666e;padding:24px 4%;padding-bottom:0;background-color:#fff;border-left:1px solid #f2f2f2;border-right:1px solid #f2f2f2;width:92%;border-top:1px solid #f2f2f2" colspan="2"><div style="font-family: 'Roboto', sans-serif;line-height:20px;color:#58666e">
                                            <label style="font-family: 'Roboto', sans-serif;line-height:20px;font-size:12px;color:#9b9b9b;font-weight:bold;text-transform:uppercase">PAYMENT FOR</label>
                                            <div style="font-family: 'Roboto', sans-serif;line-height:20px;color:#58666e"><?php echo $arrayData['invoice_type'];?></div>
                                        </div></td>
                                    <td style="font-family: 'Roboto', sans-serif;line-height:20px;padding:24px 4%;padding-bottom:0;border-right:1px solid #f2f2f2;width:3%;background-color:rgb(35,113,236);color:#ffffff"></td>
                                </tr>
                                <tr style="font-family: 'Roboto', sans-serif;line-height:20px;color:#58666e">
                                    <td style="font-family: 'Roboto', sans-serif;line-height:20px;color:#58666e;padding:24px 4%;padding-bottom:0;border-left:1px solid #f2f2f2;width:3%"></td>
                                    <td style="font-family: 'Roboto', sans-serif;line-height:20px;color:#58666e;padding:24px 4%;background-color:#fff;border-left:1px solid #f2f2f2;border-right:1px solid #f2f2f2;width:92%" colspan="2"><div style="font-family: 'Roboto', sans-serif;line-height:20px;color:#58666e">
                                            <label style="font-family: 'Roboto', sans-serif;line-height:20px;font-size:12px;color:#9b9b9b;font-weight:bold;text-transform:uppercase">ISSUED TO</label>
                                            <div style="font-family: 'Roboto', sans-serif;line-height:20px;color:#58666e">
                                                <div style="font-family: 'Roboto', sans-serif;line-height:20px;color:#58666e"> <?php echo ucwords($arrayData['user_name']);?> </div>
                                            </div>
                                        </div></td>
                                    <td style="font-family: 'Roboto', sans-serif;line-height:20px;color:#58666e;padding:0 4%;border-right:1px solid #f2f2f2;width:3%"></td>
                                </tr>
                                <tr style="font-family: 'Roboto', sans-serif;line-height:20px;color:#58666e">
                                    <td style="font-family: 'Roboto', sans-serif;line-height:20px;color:#58666e;padding:24px 4%;padding-bottom:0;border-left:1px solid #f2f2f2;width:3%"></td>
                                    <td style="font-family: 'Roboto', sans-serif;line-height:20px;color:#58666e;border-bottom:1px solid #f2f2f2;border-top:1px dashed #e5e5e5;padding:24px 4%;padding-bottom:0;background-color:#fff;border-left:1px solid #f2f2f2;border-right:1px solid #f2f2f2;width:92%;padding-left:0;padding-right:0;padding-top:0" colspan="2"><table width="100%" height="100%" style="font-family: 'Roboto', sans-serif;line-height:20px;color:#58666e;width:100%;background-color:#fafafa" border="0" cellspacing="0" cellpadding="0">
                                            <tbody style="font-family: 'Roboto', sans-serif;line-height:20px;color:#58666e">
                                                <tr style="font-family: 'Roboto', sans-serif;line-height:20px;color:#58666e">
                                                    <td style="font-family: 'Roboto', sans-serif;line-height:20px;color:#58666e;padding:24px 4%;background-color:#fff"><label style="font-family: 'Roboto', sans-serif;line-height:20px;font-size:12px;color:#9b9b9b;font-weight:bold;text-transform:uppercase"> AMOUNT PAYABLE </label>
                                                        <div style="font-family: 'Roboto', sans-serif;line-height:20px;color:#58666e;font-weight:bold;font-size:18px"> ₹<?php echo ucwords($arrayData['total_invoice_amount']);?> </div></td>
                                                    <td style="font-family: 'Roboto', sans-serif;line-height:20px;color:#58666e;text-align:right;padding:24px 4%;background-color:#fff;vertical-align:bottom" rowspan="2"><a style="font-family: 'Roboto', sans-serif;line-height:20px;text-decoration:none;padding:9px 15px;display:inline-block;border-radius:4px;white-space:nowrap;color:#ffffff;background-color:rgb(35,113,236);border:1px solid rgb(35,113,236)" href="<?php echo $arrayData['shortUrlName'];?>" target="_blank"> PROCEED TO PAY </a></td>
                                                </tr>
                                            </tbody>
                                        </table></td>
                                    <td style="font-family: 'Roboto', sans-serif;line-height:20px;color:#58666e;padding:24px 4%;padding-bottom:0;border-right:1px solid #f2f2f2;width:3%"></td>
                                </tr>
                                <tr style="font-family: 'Roboto', sans-serif;line-height:20px;color:#58666e">
                                    <td style="font-family: 'Roboto', sans-serif;line-height:20px;color:#58666e;padding:24px 4%;padding-bottom:0;border-left:1px solid #f2f2f2;width:3%"></td>
                                    <td style="font-family: 'Roboto', sans-serif;line-height:20px;color:#58666e;text-align:center;padding:24px 4%;width:92%;padding-bottom:24px;" colspan="2"><div style="font-family: 'Roboto', sans-serif;line-height:20px;font-size:12px;font-weight:bold;color:#9b9b9b"> FITPASS </div>
                                        <div style="font-family: 'Roboto', sans-serif;line-height:20px;font-size:10px;color:#9b9b9b"> 2E/8, Third Floor, Jhandewalan Extension, Jhandewalan, New Delhi, Delhi 110055 </div></td>
                                    <td style="font-family: 'Roboto', sans-serif;line-height:20px;color:#58666e;padding:24px 4%;padding-bottom:0;border-right:1px solid #f2f2f2;width:3%"></td>
                                </tr>
                               
                                <tr style="font-family: 'Roboto', sans-serif;line-height:20px;color:#58666e">
                                    <td style="font-family: 'Roboto', sans-serif;line-height:20px;color:#58666e;padding:24px 4%;padding-bottom:0;border-left:1px solid #f2f2f2;width:3%"></td>
                                    <td style="font-family: 'Roboto', sans-serif;line-height:20px;color:#58666e;padding:24px 4%;padding-bottom:0;width:92%" colspan="2"></td>
                                    <td style="font-family: 'Roboto', sans-serif;line-height:20px;color:#58666e;padding:24px 4%;padding-bottom:0;border-right:1px solid #f2f2f2;width:3%"></td>
                                </tr>
                            </tbody>
                        </table>
                    </center></td>
            </tr>
        </table>

    </body>
</html>
