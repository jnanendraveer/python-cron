
  <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,700i" rel="stylesheet">
</head>

<body>
<div style="background-color:#f5f5f5; padding-top:10px; padding-bottom:10px;">
<table cellspacing="0" align="center" cellpadding="0" style=" margin:o auto; background-color:#fff;color:#29303f;width:100%;max-width:400px;font-family: 'Roboto', sans-serif;margin:0 auto;font-size:12px">
  <tbody>
     <tr>
        <td style="width:100%; height:70px;background:url(https://fitpass-images.s3.amazonaws.com/gallery_image_header-bg_A76D.png) top center no-repeat; background-size:contain;">
        <table cellspacing="0" align="right" cellpadding="0" style="width:120px; height:40px;  padding-right:10px;">
            <tbody>   
              <tr>
              <td align="center" style="padding:5px;"><a href="https://www.facebook.com/fitpassindia" style="text-decoration:none;"><img src="https://fitpass-images.s3.amazonaws.com/gallery_image_fb_ic_8578.png" width="20" height="20"></a></td>
              <td align="center" style="padding:5px;"><a href="https://twitter.com/fitpassindia/" style="text-decoration:none;"><img src="https://fitpass-images.s3.amazonaws.com/gallery_image_tw_ic_0D2E.png" width="20" height="20"></a></td>
                <td align="center" style="padding:5px;"><a href="https://www.instagram.com/fitpassindia/" style="text-decoration:none;"><img src="https://fitpass-images.s3.amazonaws.com/gallery_image_in_ic_354E.png" width="20" height="20"></a></td>
                <td align="center" style="padding:5px;"><a href="https://fitpass.co.in/" style="text-decoration:none;"><img src="https://fitpass-images.s3.amazonaws.com/gallery_image_web_ic_9527.png" width="20" height="20"></a></td>
              </tr>
            </tbody>
          </table></td>
      </tr>
    <tr>
      <td style="padding:10px 10px 0 10px; font-family: 'Roboto', sans-serif; font-weight:500; font-size:12px;color:#222222;text-align:left;">Hey there!</td>
    </tr>
    <tr>
      <td style="padding:10px 10px 0 10px; font-family: 'Roboto', sans-serif; font-weight:400; font-size:12px;color:#222222; letter-spacing:1px; line-height:18px;text-align:left;">We received your password reset request.</td>
    </tr>
    <tr>
      <td style="padding:20px 10px 10px 10px; font-family: 'Roboto', sans-serif; font-weight:400; font-size:12px;color:#222222; letter-spacing:1px; line-height:18px;text-align:left;">Please find below the OTP to create a new password.
      <div style="font-family: 'Roboto', sans-serif; font-weight:700; font-size:14px; color:#2fb372"><?php echo $arrayData['auth_url']; ?></div>
      </td></tr>
    
    <tr>
      <td style="padding:10px 10px 0 10px; font-family: 'Roboto', sans-serif; font-weight:400; font-size:12px;color:#222222; letter-spacing:0.5px; line-height:18px;text-align:left;">The OTP is valid for 2 minutes and shouldn't be shared with anyone.</td>
    </tr>
    <tr>
      <td style="padding:10px 10px 0 10px; font-family: 'Roboto', sans-serif; font-weight:400; font-size:12px;color:#222222; letter-spacing:0.5px; line-height:18px;text-align:left;">If you did not make this request, please email us at <a href="mailto:care@fitpass.co.in" style="font-weight:400;color:#2fb372">care@fitpass.co.in</a></td>
    </tr>
     <tr>
      <td style="padding:20px 0 20px 10px; font-family: 'Roboto', sans-serif; font-weight:500; font-size:12px; color:#222222; text-align:left;">#GetSetSweat
<br/>Team <span style="font-family: 'Roboto', sans-serif; font-weight:700; font-size:12px; color:#E04e4e;">FITPASS</span></td>
    </tr>
    <tr>
      <td style="background-color:#FBFBFB;"><table cellpadding="0" cellspacing="0" style="font-size:12px;border-radius:0 3px 3px 0; width:400px;">
          <tbody>
            <tr>
              <td width="65" style="padding:20px 0 0 10px;vertical-align:top"><img src="https://fitpass-images.s3.amazonaws.com/gallery_image_contact_ic_4EB5.png" width="40" height="40" style="width:40px"></td>
              <td width="333" style="padding:20px 0 20px 0"><h3 style="margin:0;font-size:14px; color:#222222;">Get in touch with us!</h3>
                <p style="margin:5px 0"><a href="mailto:care@fitpass.co.in" style="font-weight:400;text-decoration:none;color:#222222">care@fitpass.co.in</a> &nbsp; | &nbsp; <a href="tel:011-46061468" style="font-weight:400;text-decoration:none;color:#222222">011-46061468</a></p></td>
            </tr>
          </tbody>
        </table></td>
    </tr>
    <tr>
      <td style="background-color:#ffffff; padding:10px 10px 20px 10px;" align="center"><table cellpadding="0" cellspacing="0" style="width:100%;font-size:12px">
          <tbody>
            <tr>
              <td colspan="2" style="font-weight:400; color:#10314f; padding:5px 0; font-size:14px; text-align:center;">Get fit with just a tap! <span style="font-family: 'Roboto', sans-serif; font-weight:700; font-size:14px; color:#e04e4e">GET THE APP</span></td>
            </tr>
            <tr>
              <td align="right" style="font-weight:400; color:#2c485b; padding-right:10px;"><a href="https://fitpass.co.in/app" style="text-decoration:none;"><img src="https://fitpass-images.s3.amazonaws.com/gallery_image_app_store_ic_9AA3.png" width="99" height="30" /></a></td>
              <td align="left"style="font-weight:500;color:#0e314e; padding-left:10px;"><a href="https://fitpass.co.in/app" style="text-decoration:none;"> <img src="https://fitpass-images.s3.amazonaws.com/gallery_image_play_store_AFCC.png" width="99" height="30" /></a></td>
            </tr>
          </tbody>
        </table></td>
    </tr>
    <tr>
      <td style="padding:0px 20px 20px 20px; font-family: 'Roboto', sans-serif; text-align:center; font-weight:400; font-size:10px; color:#777777;">You are receiving this email because you are one of our members and have agreed 
to sign up for our services.</td>
    </tr>
  </tbody>
</table>
</body>
</html>
